# TO DO

  You will want to download some skimmed root files for these working examples.

##  DVCS example
  
  ```bash
  wget -P dataIn/ https://userweb.jlab.org/~fcao/data/root/eg6/pass_kinfitv2/eg6_coh_dvcs_pass_fit_plus_3000.root
  ```

##  Pi0 example
  
  ```bash
  wget -P dataIn/ https://userweb.jlab.org/~fcao/data/root/eg6/pass_kinfitv2/eg6_coh_pi0_pass_cutsOrFit.root
  ```

