## About

  Frank Cao's Kinematic Fitter (FCKinFitter) library is made to make kinematic fitting easy to use and general. The library is made of three classes that help the user build a custom kinematic fitter and to see the results:
  1. `KinFitter`
     * The meat of the library that does the linear algebra and minimization to get out the fitted variables from input measured variables and constraints
  2. `KFParticle`
     * A(nother) particle class that has some members and member functions that talk to the `KinFitter` class to help build the constraints
  3. `KFMonitor`
     * The Kin. Fitter Monitor that produces many of the relevant histograms that are important to see to evaluate the quality of the fit
  3. `Kinematics`
     * General class to handle build kinematic and exclusivity variables from a set of Lorentz vectors (used to help KFMonitor construct meaningful plots)
  
## Installing and Setting Up

  Check the [wiki](https://gitlab.com/franktcao/FCKinFitter/wikis/home) to see how to install and use the library on your local machine.
  
## Working Examples

  Once installed and things are set up, you can find some working examples in the `examples/` directory. These scripts show you how to use this kin. fitter library in a few different ways. Inside that directory, you will see the example scripts:

  * `src/collect_coh_dvcs.cxx` 
     * Use kinematic fitting to select DVCS events from really skimmed eg6 data
  * `src/collect_coh_pi0.cxx` 
     * Use kinematic fitting to select coherent pi0 events from really skimmed eg6 data
  
  In order for these examples to work, you will need to download the skimmed eg6 data (Check the README in `examples/` on where to put them):

  * https://userweb.jlab.org/~fcao/data/root/eg6/pass_kinfitv2/eg6_coh_dvcs_pass_fit_plus_3000.root
  * https://userweb.jlab.org/~fcao/data/root/eg6/pass_kinfitv2/eg6_coh_pi0_pass_cutsOrFit.root

