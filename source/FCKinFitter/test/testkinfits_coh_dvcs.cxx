#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TRint.h"
#include "ROOT/RDataFrame.hxx"
#include <string>
#include "KinFitter.h"
#include "res-errs_clas.h"

std::vector<KFParticle> KFParticles;
//______________________________________________________________________________

void updateKFPs( std::vector<KFParticle> &KFPs, const std::vector<TLorentzVector> &Ps, int detID, double vz_e ){
    TLorentzVector P_e    = Ps[0];
    TLorentzVector P_rec  = Ps[1];
    TLorentzVector P_prod = Ps[2];
    //P_e   .Print();
    //P_rec .Print();
    //P_prod.Print();
    //cout << endl;

    KFPs[0] .SetP( P_e );
    KFPs[0] .SetSigmas( (std::vector<double>) getResErr_DC ( P_e )    );

    KFPs[1] .SetP( P_rec );
    KFPs[1] .SetSigmas( (std::vector<double>) getResErr_RTPC( P_rec ) );

    KFPs[2] .SetP( P_prod );
    KFPs[2] .SetSigmas( (std::vector<double>) getPhotErrors( detID, P_prod, vz_e ) ); 
}

//________________________________________________________________________________________________________________________________

TLorentzVector getUpdatedLV( const std::vector<double> &y, const std::vector<double> &x, int ikf, int ivar = -1 ){
  // Adds up the measured or unmeasured KFParticles
  // The indices hold the recipe that connects the vectors x and y to the constraints

  KFParticle KFP = KFParticles[ikf];
  int ipart = KFP.ipart_yx;

  // Get the relevant p, theta, and phi
  std::vector<double> pthetaphi;
  if( KFP.isMeas == 1 ) pthetaphi.insert( pthetaphi.end(), y.begin() + ipart, y.begin() + ipart + 3 ); 
  else                  pthetaphi.insert( pthetaphi.end(), x.begin() + ipart, x.begin() + ipart + 3 ); 
  
  // Explicitly assign them
  double &p     = pthetaphi[0];
  double &theta = pthetaphi[1];
  double &phi   = pthetaphi[2];
  double M      = KFP.P.M();
  
  TVector3 p3;
  TLorentzVector P;
  // if ivar is positive, get the derivative
  if( ivar >= 0 ){
    double h = 1E-5;

    TLorentzVector P_hi;
    pthetaphi.at(ivar) += h;
    p3.SetMagThetaPhi( p, theta, phi );
    P_hi.SetVectM( p3, M );
    pthetaphi.at(ivar) -= h; // Undoing the substraction (not needed since it'll go out of scope anyway)

    TLorentzVector P_lo;
    pthetaphi.at(ivar) -= h;
    p3.SetMagThetaPhi( p, theta, phi );
    P_lo.SetVectM( p3, M );
    pthetaphi.at(ivar) += h; // Undoing the substraction (not needed since it'll go out of scope anyway)

    // dPdx = ( P(x+h) -P(x-h) )/ 2h
    P = 1/(2*h) * (P_hi - P_lo);
  }
  else{
    p3.SetMagThetaPhi( p, theta, phi );
    P.SetVectM( p3, M );
  }

  return P;
}
//______________________________________________________________________________

std::vector<double> getConstraints_dvcs( const std::vector<double> &y, const std::vector<double> &x, int ipart, int ivar = -1 ){
  // Returns the constraint: P_beam + P_targ - ( P_e + P_he4 + P_dvcs ) if ipart == -1
  // Returns the derivative of constraint w.r.t. the "ivar-th" variable of kfparticle ipart: P_beam + P_targ - ( P_e + P_he4 + P_dvcs )
  enum KFPs { e, he4, phot };
  std::vector<double> result(4);
  TLorentzVector P_const;
  
  std::vector<KFPs> indices = { e,  he4,  phot };
  std::vector<int>  signs   = {-1, -1, -1};
  // In this condition when just evaluating the constraints
  TLorentzVector P_beam = TLorentzVector( 0, 0, 6.064, 6.064 );
  TLorentzVector P_targ = TLorentzVector( 0, 0, 0,     3.74 );
  if( ipart < 0 ) P_const = P_beam + P_targ;
  for( int ii = 0; ii < signs.size(); ii++ ){ 
    // if ipart is not default, then check to see if ipart is one of the participating particles
    if( ipart >= 0 && ipart != indices.at( ii ) ) continue; 
    int sign = signs.at( ii );
    P_const += sign * getUpdatedLV( y, x, ii, ivar ); 
  }
  for( int ii = 0; ii < 4; ii++ ) result.at(ii) = P_const[ii]; 

  return result;
}

//________________________________________________________________________________________________________________________________
//R__LOAD_LIBRARY(libKinFitter)


//void setUpTreeOut(){
//  t_out = new TTree( "eg6_"+recname + "_" + prodname, "EG6 " + recname + ". " + prodname );
//  t_out = (TTree*) t_in->CloneTree(0);
//  TBranch* b_Q2      = t_out->Branch( "Q2",      &Q2      );
//  TBranch* b_x       = t_out->Branch( "x",       &x       );
//  TBranch* b_t       = t_out->Branch( "t",       &t       );
//  TBranch* b_phi     = t_out->Branch( "phi",     &phi     );
//  TBranch* b_Q2_fit  = t_out->Branch( "Q2_fit",  &Q2_fit  );
//  TBranch* b_x_fit   = t_out->Branch( "x_fit",   &x_fit   );
//  TBranch* b_t_fit   = t_out->Branch( "t_fit",   &t_fit   );
//  TBranch* b_phi_fit = t_out->Branch( "phi_fit", &phi_fit );
//}

//https://userweb.jlab.org/~fcao//data/root/eg6/pass_kinfitv2/eg6_coh_dvcs_pass_fit_plus_3000.root
//https://userweb.jlab.org/~fcao//data/root/eg6/pass_kinfitv2/eg6_coh_pi0_pass_cutsOrFit.root

int main(int argc, char *argv[]){

  //auto d1 = d0
  //              .Define("px", [](TLV e) { return e.Px(); }, {"P_e"})
  //              .Define("py", [](TLV e) { return e.Py(); }, {"P_e"})
  //              .Define("pz", [](TLV e) { return e.Pz(); }, {"P_e"});
  
  TRint*  app    = new TRint("App", &argc, argv);
  ROOT::EnableImplicitMT(4); // Tell ROOT you want to go parallel
  
  double clc = 0.4;

  std::cout << "If file is missing, run \n"
               "wget "
               "https://userweb.jlab.org/~fcao//data/root/eg6/pass_kinfitv2/"
               "eg6_coh_dvcs_pass_fit_plus_3000.root\n";
  
  using TLV = TLorentzVector;
  ROOT::RDataFrame d0("eg6_coh_dvcs", "eg6_coh_dvcs_pass_fit_plus_3000.root",
                      {"P_e", "P_rec", "P_prod", "detID"});
  
  std::function< std::vector<double>( const std::vector<double>&, const std::vector<double>&, int, int ) > eval_constraints = 
      [=]( const std::vector<double> &y, const std::vector<double> &x, int ipart, int ivar ){ return getConstraints_dvcs( y, x, ipart, ivar ); };

  KFParticles.emplace_back( TLorentzVector(), std::vector<double>(), "e"            , 1, 1 );
  KFParticles.emplace_back( TLorentzVector(), std::vector<double>(), "^{4}He"       , 1, 1 );
  KFParticles.emplace_back( TLorentzVector(), std::vector<double>(), "#gamma_{DVCS}", 1, 1 );
  
  int nconstraints = 4;

  auto d1 = d0.Define("confLevel", 
                      [&](const TLV& e, const TLV& r, const TLV& g, int detID, double vz_e) {
                        std::vector<TLV> Ps = {e, r, g};
                        updateKFPs( KFParticles, Ps, detID, vz_e );
                        KinFitter fit = KinFitter( KFParticles, eval_constraints, nconstraints );
                        fit.ProcessFit(); 
                        double confLevel = fit.confLevel;
                        return confLevel;
                      },
                      {"P_e", "P_rec", "P_prod", "detID", "vz_e"});

  auto d2 = d1.Filter("confLevel>0.4");//[](const double& cl) { return (cl > 0.4); }, {"kinfit_result"});

  auto h_CL_0 = d1.Histo1D( {"h_cl_0", "Confidence Level; CL; counts/100", 100, 0,1}, {"confLevel"});
  auto h_CL_1 = d2.Histo1D( {"h_cl_1", "Confidence Level; CL; counts/100", 100, 0,1}, {"confLevel"});

  h_CL_0->DrawCopy();
  h_CL_1->SetFillColorAlpha(kBlue, 0.35);
  h_CL_1->DrawCopy("same");

  auto n_events      = d1.Count();
  auto n_events_pass = d2.Count();

  std::cout << *n_events      <<  " events \n";
  std::cout << *n_events_pass <<  " passed \n";


  app->Run();
}

  //gSystem->mkdir("out/root",true);
  //gSystem->mkdir("out/images/pdf",true);
  //std::string rectitle = "^{4}He";
  //std::string recname  = "coh";
  //TString fname = "dataIn/eg6_coh_pi0_pass_cutsOrFit.root";
  //fname = "dataIn/eg6_coh_dvcs_pass_fit_plus_3000.root";

  //TFile* f_in = new TFile("eg6_coh_dvcs_pass_fit_plus_3000.root", "READ");
  //TTree* t_in = (TTree*)gROOT->FindObject("eg6_coh_dvcs");
  //if(!t_in) {
  //  std::cerr << " Tree not found!\n";
  //  std::exit(-1);
  //}
  //t_in->SetBranchAddress("helicity", &hel);
  //t_in->SetBranchAddress("vz_e",     &vz_e);
  //t_in->SetBranchAddress("P_e",      &P_e);
  //t_in->SetBranchAddress("P_rec",    &P_rec);
  //t_in->SetBranchAddress("P_prod",   &P_prod);
  //t_in->SetBranchAddress("detID",    &detID);
  //
