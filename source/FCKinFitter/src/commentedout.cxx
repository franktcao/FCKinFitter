

//std::vector<KFParticle> KinFitter::FromYXtoKFP( const VectorXd &y, const VectorXd &x ){
//  
//}

//std::tuple<VectorXd, VectorXd> KinFitter::FromKFPtoYX( const std::vector<KFParticle> &Particles, bool isConstructCovMat = true ){
//  // Converts the vector of KFParticles to measured and unmeasured vectors y and x
//
//  //return std::move( std::make_tuple( y, x ) );
//  return  std::make_tuple( y, x );
//}
//TVector3 KinFitter::FromEigenToLorentz( const VectorXd &eigen ){
//  TVector3 vec3;
//  vec3.SetMagThetaPhi( eigen(0), eigen(1), eigen(2) );
//  //return std::move( vec3 );
//  return vec3;
//}

//int KinFitter::StageInputs( const std::vector<KFParticle> &KFParts, const std::vector<std::function> &set_of_constraints, const MatrixXd &C_eta ){
//  return 1;
//}

//TLorentzVector evalConstraintsFromString( const std::string constraints ){
//   
//
//}
//
//void BuildConstraintsFromString( const std::string &constraint_str ){
//  // Sets the set_of_constraint variables from reading in a string
//
//  
//  std::string str = constraint_str;
//  
//  // Removes extra spaces
//  std::string::iterator end_pos = std::remove(str.begin(), str.end(), ' ');
//  str.erase(end_pos, str.end());
//  
//  // Breaks up the string into its constraints, which should be separated by semi-colons (;)
//  std::vector<std::string> constraints = TokenizeString( str, ";" );
//  
//  // Loops over set of constraints and separates out the inital and final state particles
//  std::vector<std::string> fin_state_particles;    
//  std::vector<std::string> init_state_particles;     
//  for( auto constraint : constraints ){
//    std::vector<std::string> tokens = TokenizeString( constraint, "->" );
//    init_state_particles.push_back( tokens[0] );
//    fin_state_particles.push_back(  tokens[1] );
//    
//    std::function<double( const VectorXd&, const VectorXd&, int )> func 
//      = [&]( const VectorXd &y, const VectorXd &x, int )
//      {
//        double result;
//        std::vector<KFParticle> Particles = UpdateKFParticles( y, x );
//        for( auto ii : init_state_particles ){
//          P_constr += GetLorentzFromString( ii, Particles, 0 );
//        }
//        for( auto ii : fin_state_particles ){
//          P_constr -= GetLorentzFromString( ii, Particles, 0 );
//        }
//        //return std::move(P_constr);
//        return P_constr;
//      }
//      set_of_constraints.push_back( func );
//
//  }
//}
//
//std::vector<std::string> TokenizeString( const std::string &str, const std::string &delimeter ){
//  size_t last = 0; 
//  size_t next = 0; 
//  std::string s = str;
//  std::vector<string> tokens;
//  //string delimiter = "->";
//  string token;
//  while ((next = s.find(delimiter, last)) != string::npos) { 
//    token = s.substr(last, next-last);
//    //cout << token << endl; 
//    last = next + 1; 
//    //cout << "pushing back token : " << token << endl;
//    tokens.push_back(token);
//  } 
//  token = s.substr(last);
//  //cout << token << endl;
//  //cout << "pushing back token : " << token << endl;
//  tokens.push_back(token);
//
//}
//
//double GetComponentFromString( const std::string &token, const std::vector<KFParticle> & Particles, int icomp ){
//
//  TLorentzVector P = GetLorentzFromString( token, Particles );
//  
//  return P[icomp];
//}
//
//TLorentzVector GetLorentzFromString( const std::string &token, const std::vector<KFParticle> & Particles ){
//
//  TLorentzVector P;
//  switch(token){ 
//    case "[b]" : 
//      P = P_beam; 
//      //return std::move(P); 
//      return P; 
//    case "[t]" : 
//      P = P_targ; 
//      //return std::move(P); 
//      return P; 
//    //case 'N' : 
//    //  cout << "No"; 
//    //  break; 
//    default: 
//    //  cout << "Invalid response"; 
//  }
//
//  int index = GetIndexFromToken( token );
//  
//  P = Particles[ abs(index) ].P;
//  
//  //return std::move(P);
//  return P;
//}
//
//std::string get_str_between_two_str(const std::string &s, const std::string &start_delim, const std::string &stop_delim){
//  // From user "ifma" on stack overflow
//  unsigned first_delim_pos = s.find(start_delim);
//  unsigned end_pos_of_first_delim = first_delim_pos + start_delim.length();
//  unsigned last_delim_pos = s.find(stop_delim);
//
//  return s.substr(end_pos_of_first_delim,
//      last_delim_pos - end_pos_of_first_delim);
//}
//
//int GetIndexFromToken(const std::string &s ){
//  string index_str = get_str_between_two_str( s, "[", "]" );
//  return stoi( index_str );
//
//}

//void SetP_targ( const TLorentzVector &P ){
//  P_targ = P;
//}
//
//void SetM_targ( Double_t M ){
//  P_targ.SetE(M);
//}
//
//void SetP_beam( const TLorentzVector &P ){
//  P_beam = P;
//}
//void SetBeam( Double_t E ){
//  P_beam.SetE(E);
//  P_beam.SetZ(E);
//}

  
  
  ////    _std2 denotes 2-dimensional std::vector
  //std::vector<double> c_std;
  //std::vector< std::vector<double> >  B_std2, A_std2;
  //std::tie( c_std, B_std2, A_std2 ) = ConstructCBA( y, x );
  // std->eig
  //// 1D vectors that can be fed into Eigen Matrices
  //using std::begin;
  //using std::end;
  
  //std::vector<double> B_std;
  //for( auto b : B_std2 ) B_std.insert( B_std.end(), b.begin(), b.end() ); 
  //
  //std::vector<double> A_std;
  //for( auto a : A_std2 ) A_std.insert( A_std.end(), a.begin(), a.end() ); 
  //
  //VectorXd c = Eigen::Map< VectorXd >( c_std.data(), c_std.size() );
  //Map<MatrixXdRM> B( B_std.data(), B_std2.size(), B_std2[0].size() );
  //Map<MatrixXdRM> A( A_std.data(), A_std2.size(), A_std2[0].size() );
  //std::tie( c_std, B_std2, A_std2 ) = ConstructCBA( y, x );
  
  
    //double      confLevel; 
    //vector_d    pulls; 
    //vector_KFP  KFParticlesOut;
    //
    //vector_KFP  KFParticlesIn;
    //vector_func set_of_constraints;
    
  // Construct Covariance Matrix
  //std::vector<double> sigma2s_eta = Eigen::Map<std::vector<double>>( sigmas.data(), sigmas.size() );
  //sigma2s_eta = sigma2s_eta.cwiseAbs2();
  //C_eta = MatrixXd( sigma2s_eta.asDiagonal() );

  // Squares the sigmas for the covariance matrix
  //for_each( sigmas.begin(), sigmas.end(), []( double &val ){ val *= val; } );
  
  



  //int i_x = 0;
  //int i_y = 0;
  //for( unsigned j = 0; j < KFParticlesIn.size(); j++ ){
  //KFParticle Part = KFParticlesIn[j];
  //int idx = 0;
  //if( Part.isMeas ){
  //i_y++;
  //idx = i_y-1;
  //}
  //else{
  //i_x++;
  //idx = i_x-1;
  //}
  //double p     = x[3*idx + 0];
  //double theta = x[3*idx + 1];
  //double phi   = x[3*idx + 2];
  //double M     = Part.P.M();
  //double E     = sqrt( p*p + M*M );
  //int sign = 0;
  //MatrixXd D_block  << sin(theta)*cos(phi),  p*cos(theta)*cos(phi), -p*sin(theta)*sin(phi),
  //                     sin(theta)*sin(phi),  p*cos(theta)*sin(phi),  p*sin(theta)*cos(phi),
  //                     cos(theta)         , -p*sin(theta)         ,  0                    ,
  //                     p/E                ,  0                    ,  0                    ;
  //                     
  //
  
  
  //std::vector<double> c(n_constraints); 
  //std::vector< std::vector<double> > B( n_constraints, std::vector<double>(n_vars_y) ); 
  //std::vector< std::vector<double> > A( n_constraints, std::vector<double>(n_vars_x) ); 
  
                                                             
                                                                                
                                                                                                           
 

                                                                                                                                    

                                                                                                                            
                    
                    

                  

                      
                
                         
                                                      

                      
                
                                                                              
                                                      

                   
                
                   
                

                                   
  

////________________________________________________________________________________________________________________________________
//
//
//TLorentzVector KFParticle::GetLVFromPThPh( vector_d &pthetaphi ){
//  // Get the Lorentz vector from p, theta, and phi, using the current mass
//  TLorentzVector result;
//
//  double &p     = pthetaphi.at(0);
//  double &theta = pthetaphi.at(1);
//  double &phi   = pthetaphi.at(2);
//  double M      = P.M();
//  
//  TVector3 p3;
//  p3.SetMagThetaPhi( p, theta, phi );
//  result.SetVectM( p3, M );
//  return result;
//}
//
//________________________________________________________________________________________________________________________________
//
//TLorentzVector KFParticle::GetDerLVFromPThPh( vector_d &pthetaphi, int ivar ){
//  // Get the Lorentz vector where each component i, is the derivative of the i-th component (px, py, pz, E), w.r.t. p, theta, or phi 
//  TLorentzVector result;
//  
//  double &p     = pthetaphi.at(0);
//  double &theta = pthetaphi.at(1);
//  double &phi   = pthetaphi.at(2);
//  double M      = P.M();
//
//  TVector3 p3;
//  double h = 1E-5;
//  
//  TLorentzVector P_hi;
//  pthetaphi.at(ivar) += h;
//  p3.SetMagThetaPhi( p, theta, phi );
//  P_hi.SetVectM( p3, M );
//
//  TLorentzVector P_lo;
//  pthetaphi.at(ivar) -= 2*h; // Undoing the addition as well as setting the low
//  p3.SetMagThetaPhi( p, theta, phi );
//  P_lo.SetVectM( p3, M );
//
//  result = P_hi - P_lo;
//  result *= 1/(2*h);
//  
//  return result;
//  //pthetaphi.at(ivar) += h; // Undoing the substraction (not needed since it'll go out of scope anyway)
//}
//
////________________________________________________________________________________________________________________________________
//
//std::tuple<TLorentzVector, TVector3> KFParticle::GetDerFromKFVars( vector_d &kfvars, int ivar, const TLorentzVector &P ){
//  // Get the Lorentz vector where each component i, is the derivative of the i-th component (px, py, pz, E), w.r.t. p, theta, or phi 
//  TLorentzVector result;
//  
//  TVector3 p3;
//  TVector3 V;
//  double h = 1E-5;
//  
//  TLorentzVector P_hi;
//  kfvars.at(ivar) += h;
//  p3 = std::get<TLorentzVector>( GetPhysVars( kfvars, P ) ).Vect();
//  P_hi.SetVectM( p3, M );
//
//  TLorentzVector P_lo;
//  kfvars.at(ivar) -= 2*h; // Undoing the addition as well as setting the low
//  p3 = std::get<TLorentzVector>( GetPhysVars( kfvars, P ) ).Vect();
//  P_lo.SetVectM( p3, M );
//
//  result = P_hi - P_lo;
//  result *= 1/(2*h);
//  
//  return std::make_tuple( P, TVector3() );
//}
//
//________________________________________________________________________________________________________________________________

//void KFParticle::SetKFVars( string icoor_p = "spherical", string icoor_v = "cartesian" ){
//  // Get the kin. fit variables from the 4 momentum (P) and/or the vertex (V)
//  // Momentum Components
//  /// Momentum in Spherical
//  KF_vars.push_back( P.P() ); 
//  KF_vars.push_back( P.Theta() ); 
//  KF_vars.push_back( P.Phi() ); 
//  ////// Momentum in Cartesian 
//  //KF_vars.push_back( P.X() ); 
//  //KF_vars.push_back( P.Y() ); 
//  //KF_vars.push_back( P.Z() ); 
//  ////// Momentum in Cylindrical 
//
//  //// Vertex components
//  ///// Vertex in Cartesian
//  //KF_vars.push_back( V.X() ); 
//  //KF_vars.push_back( V.Y() ); 
//  //KF_vars.push_back( V.Z() ); 
//
//}
////________________________________________________________________________________________________________________________________
//
//void KFParticle::SetSigmas( const vector_d &psigs, const vector_d &vsigs  ){ 
//  P_sigmas = ; 
//  V_sigmas = vsigs; 
//  SetCovMat( psigs, );
//}
//
//________________________________________________________________________________________________________________________________

//std::vector<double> KFParticle::GetKFVars( ){
//  // Get the kin. fit variables from the 4 momentum (P) and/or the vertex (V)
//
//  std::vector<double> result;
//
//  // Momentum Components
//  switch(P_coor_sys) {
//    case "spherical":
//      result.push_back( P.P() ); 
//      result.push_back( P.Theta() ); 
//      result.push_back( P.Phi() ); 
//      break;
//    case "cartesian":
//      result.push_back( P.X() ); 
//      result.push_back( P.Y() ); 
//      result.push_back( P.Z() ); 
//      break;
//    case "cylindrical":
//      result.push_back( P.Pt() ); 
//      result.push_back( P.Phi() ); 
//      result.push_back( P.Z() ); 
//      break;
//    //default: //optional
//  }
//
//  // Vertex components
//  switch(V_coor_sys) {
//    case "spherical":
//      result.push_back( V.P() ); 
//      result.push_back( V.Theta() ); 
//      result.push_back( V.Phi() ); 
//      break;
//    case "cartesian":
//      result.push_back( V.X() ); 
//      result.push_back( V.Y() ); 
//      result.push_back( V.Z() ); 
//      break;
//    case "cylindrical":
//      result.push_back( V.Pt() ); 
//      result.push_back( V.Phi() ); 
//      result.push_back( V.Z() ); 
//      break;
//    //default: //optional
//  }
//
//  return result;
//}

//// Constructor with extra constraints (other than energy and momentum conservation... like an invariant mass)
////KinFitter::KinFitter( const std::vector<KFParticle> &Particles, const std::vector<std::function< double( const std::vector<double> &, const std::vector<double>&) >> &constraints )
//KinFitter::KinFitter( vector_KFP &Particles, const ConstraintBuilder &c )
//  : 
//    chi2( 0 ),
//    iter( 0 ),
//    constraints(c)
//{
//  
//  std::vector<Double_t> sigmas;
//  for( auto &p: Particles ) AddParticle( p, sigmas );
//  
//  // copy after setting up so that it can know what ipart_yx is 
//  KFParticlesIn    = Particles;
//
//  // Set up Covariance matrix
//  C_eta = vector_d2( eta.size(), vector_d(eta.size()) );
//  for( unsigned ii = 0; ii < C_eta.size(); ii++ ) C_eta[ii][ii] = pow( sigmas[ii], 2 );
//  epsilon         = vector_d( eta.size(), 0 );
//  sigma2s_epsilon = vector_d( eta.size(), 0 );
//  
//  n_constraints = constraints.n_constraints;
//  ndf = n_constraints - x0.size(); 
//  if( ndf < 0 ){ cout << "Number of Degrees of Freedom < 0! : " << ndf << endl; }
//  else{ isValid = true; }
//  
//}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//KinFitter::KinFitter( vector_KFP &Particles, const std::function<std::vector<double>(const std::vector<double>&, const std::vector<double>&, int)>  &constraints, int ncons )
//  : 
//    chi2( 0 ),
//    iter( 0 )
//{
//  eval_constraints = constraints;
//  
//  // To be updated
//
//  std::vector<Double_t> sigmas;
//  for( auto &p: Particles ) AddParticle( p, sigmas );
//  // copy after setting up so that it can know what ipart_yx is 
//  KFParticlesIn    = Particles;
//  // Set up Covariance matrix
//  C_eta = vector_d2( eta.size(), vector_d(eta.size()) );
//  for( unsigned ii = 0; ii < C_eta.size(); ii++ ) C_eta[ii][ii] = pow( sigmas[ii], 2 );
//  epsilon         = vector_d( eta.size(), 0 );
//  sigma2s_epsilon = vector_d( eta.size(), 0 );
//  
//  n_constraints = ncons;
//  ndf = n_constraints - x0.size(); 
//  if( ndf < 0 ){ cout << "Number of Degrees of Freedom < 0! : " << ndf << endl; }
//  else{ isValid = true; }
//  
//}
////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//KinFitter::KinFitter( vector_KFP &Particles, const std::vector< std::function<double(const std::vector<double>&, const std::vector<double>&)> > &constraints )
//  : 
//    chi2( 0 ),
//    iter( 0 )
//{
//  set_of_constraints = constraints;
//
//
//  std::vector<Double_t> sigmas;
//  for( auto &p: Particles ) AddParticle( p, sigmas );
//  // copy after setting up so that it can know what ipart_yx is 
//  for( int ii = 0; ii < Particles.size(); ii++ ){ Particles[ii].ipart_KFP = ii; } 
//  KFParticlesIn  = Particles;
//  // Set up Covariance matrix
//  C_eta = vector_d2( eta.size(), vector_d(eta.size()) );
//  for( unsigned ii = 0; ii < C_eta.size(); ii++ ) C_eta[ii][ii] = pow( sigmas[ii], 2 );
//  epsilon         = vector_d( eta.size(), 0 );
//  sigma2s_epsilon = vector_d( eta.size(), 0 );
//  
//  n_constraints = set_of_constraints.size();
//  ndf = n_constraints - x0.size(); 
//  if( ndf < 0 ){ cout << "Number of Degrees of Freedom < 0! : " << ndf << endl; }
//  else{ isValid = true; }
//  
//}
//
    //int n_sigs = Part.cov_mat.size();
    //if( n_sigs < 3 ){ std::cout << "This will fail: If measured, you need the errors (3 for p, theta, and phi)!! You have : " << n_sigs  << std::endl; }
    //for( auto row : Part.cov_mat ){ for( auto el : row ) cout << el << " " ; cout << endl; }
    //cout << endl;
  
  //std::vector<double> C_eta_std;
  //for( auto a : C_eta ) C_eta_std.insert( C_eta_std.end(), a.begin(), a.end() ); 
  // For SOME reason, the default map is NOT row-major
  //using MatrixXdRM = Matrix<double, Dynamic, Dynamic, RowMajor>;
  //Map<MatrixXdRM> C_eta_eig( C_eta_std.data(), C_eta.size(), C_eta[0].size() );

  
  //std::tie( c, B, A ) = constraints.GetCBA( y, x );
  //std::vector<double> c_std, B_std, A_std;
  //std::tie( c_std, B_std, A_std ) = constraints.GetCBA( y, x );
  //VectorXd c  = Map<VectorXd>( c_std.data(), c_std.size() );
  //Map<MatrixXdRM> B( B_std.data(), n_constraints, y.size() );
  //MatrixXd A; 
  //if( x.size() > 0 )  A = Map<MatrixXdRM>( A_std.data(), n_constraints, x.size() );  
  //else              { A = MatrixXd( n_constraints, n_constraints ); A.setZero(); }// This size for multiplication reasons
    
    //int ipart    = KFP.ipart_yx;
    //int n_kfvars = KFP.n_kfvars;
    // 
    //vector_d kfvars(n_kfvars);
    //if( KFP.isMeas == 1 )  kfvars.assign( y.begin() + ipart + 0, y.begin() + ipart + n_kfvars );
    //else                   kfvars.assign( x.begin() + ipart + 0, x.begin() + ipart + n_kfvars );
  
  
  //if( KFP.isMeas > 0 ){
  //  KFP.ipart_yx = eta.size();
  //  eta.insert( eta.end(), kfvars.begin(), kfvars.end() );
  //  
  //  ExtendCovMat( KFP.cov_mat );
  //}
  //else{
  //  KFP.ipart_yx = x0.size();
  //  x0.insert( x0.end(), kfvars.begin(), kfvars.end() );
  //}

    //vector_d                 kfvars;                    // Kin. Fit variables (x or y) stored in vector form
    //vector_d                 P_sigmas;                  // Errors/widths
    ////SOMETHING                GetKFVarsFromP()           // Outside function that defines how to get the KFVars from the momentum
    //vector_d                 V_sigmas;                  // Vertex Errors/widths
    ////SOMETHING                GetKFVarsFromV()           // Outside function that defines how to get the KFVars from the vertex 

    //vector_d                 KF_vars;                   // Get measured/unmeasured variables (default is {p, theta, phi} but it can be {rho, z, phi}, or {vx, vy, vz}, etc
    //std::string              P_coor_sys = "spherical";  // Coordinate system: "cartesian", "spherical" (default), "cylindrical", "" (empty) don't use these measured variables
    //std::string              V_coor_sys = "";           // Coordinate system: "cartesian", "spherical", "cylindrical", "" (default) don't use these measured variables
    //void SetSigmas( const vector_d &psigs, const vector_d &vsigs = {} );
    //std::function< std::tuple<TLorentzVector, TVector3>( vector_d &, const TLorentzVector &, int ) >  GetPhysVarsDers
    //  = [&, this]( vector_d &_kfvars, const TLorentzVector &_P, int _ivar ){
    //    TLorentzVector dP;
    //    TVector3       dV;

    //    double h = 1E-5;

    //    TLorentzVector P_hi;
    //    TVector3 V_hi;
    //    _kfvars.at(_ivar) += h;
    //    std::tie( P_hi, V_hi ) = GetPhysVars( _kfvars, _P );
    //    //P_hi.Print();

    //    TLorentzVector P_lo;
    //    TVector3 V_lo;
    //    _kfvars.at(_ivar) -= 2*h; // Undoing the addition as well as setting the low
    //    std::tie( P_lo, V_lo ) = GetPhysVars( _kfvars, _P );

    //    dP = P_hi - P_lo;
    //    dP *= 1/(2*h);
    //    dV = V_hi - V_lo;
    //    dV *= 1/(2*h);

    //    return std::make_tuple( dP, TVector3() );
    //  };

    //int n_kfvars = KFP.GetNKFVars();
    //std::vector<double> kfvars;
    //std::vector<double> xy;
    //
    //if( KFP.isMeas == 1 ) xy = y;
    //else                  xy = x;
    //auto ipart0 = xy.begin() + KFP.ipart_yx;
    //kfvars.assign( ipart0, ipart0 + n_kfvars );

  //std::vector<double> kfvars;
  //std::vector<double> xy;

  //if( KFP_pi0.isMeas == 1 ) xy = y;
  //else                      xy = x;
  //auto ipart0 = xy.begin() + KFP_pi0.ipart_yx;
  //int n_kfvars = KFP_pi0.GetNKFVars();
  //kfvars.assign( ipart0, ipart0 + n_kfvars );
    
    //int i_part_y = 0;
    //int i_part_x = 0;
    //int ipart_yx = KFP.ipart_yx;
    //int n_kfvars = KFP.n_kfvars;
    //if( KFP.isMeas == 1 ){
    //  // Construct B from constraints
    //  for( int jj = ipart_yx; jj < ipart_yx + n_kfvars; jj++ ) B.col(jj)  = Map<VectorXd>( eval_constraints( y, x, ipart, jj%n_kfvars ).data(), n_constraints ); 
    //  i_part_y++;
    //}
    //else{
    //  // Construct A from constraints
    //  for( int jj = ipart_yx; jj < ipart_yx + n_kfvars; jj++ ) A.col(jj)  = Map<VectorXd>( eval_constraints( y, x, ipart, jj%n_kfvars ).data(), n_constraints ); 
    //  i_part_x++;
    //}
  //std::vector< std::function<double(const std::vector<double>& , const std::vector<double>& )> >           set_of_constraints;
  //std::function<std::vector<double>(const std::vector<double>& , const std::vector<double>&, int )>        eval_constraints;
  //ConstraintBuilder constraints;
  //Constraints constraints;

  //template<typename Constraints>
  //  struct constraints{
  //    Constraints evaluate;
  //  };

  // KinFitter( vector_KFP &Particles, const std::function<vector_d(const vector_d&, const vector_d&, int, int )>  &constraints_functs, int ncons, const vector_d2 &cov_mat );
  
  //template<typename Constraints>
  //constraints<typename std::decay<Constraints>::type> build_constraints(Constraints&& c){ return { std::forward<Constraints>(c) }; }
  
  //template<typename Constraints>
  //KinFitter( vector_KFP &Particles, const std::function<std::vector<double>(const std::vector<double>&, const std::vector<double>&, int)>        &constraints_functs, int ncons );
  //KinFitter( vector_KFP &Particles, const std::vector< std::function<double(const std::vector<double>&, const std::vector<double>&)> >           &constraints_functs );







  //// Add padding to the rows
  //vector_d zeros( cov_mat_compact.size(), 0 );
  //if( _isCovMatDiag ){
  //  //!!!
  //  for( auto e : _cov_mat ){ 
  //    cov_mat_compact.insert( cov_mat_compact.end(), zeros.begin(), zeros.end() ); 
  //    cov_mat_compact.push_back( e );
  //    zeros.push_back( 0 );
  //  }
  //}
  //else{
  //  // nrow is the number of rows for a given number of elements of pascal's triangle
  //  int nrow =  (1 + sqrt( 1 + 8 * _cov_mat.size() ) )/2 - 1;
  //  for( int irow = 0; irow < nrow; irow++ ){
  //    //// nlast is the total number of elements above this row of pascal's triangle
  //    int nlast = ( irow * (irow + 1) )/2; 
  //    int ncol  = irow + 1;
  //    cov_mat_compact.insert( cov_mat_compact.end(), zeros.begin(), zeros.end() ); 
  //    //cout << "irow " <<  irow << " / " << nrow << endl;
  //    for( int icol = 0; icol < ncol; icol++){
  //      //cout << "\t icol " <<  icol << " / " << ncol << endl;
  //      //cout << "\t nlast " <<  nlast  << endl;
  //      cov_mat_compact.push_back( _cov_mat.at(nlast+ icol) );
  //      //for( auto cc : _cov_mat ) cout << cc << " " ; cout << endl;
  //    }
  //    
  //  }
  //}
  //for( auto cc : cov_mat_compact ) cout << cc << " " ; cout << endl;

  //cov_mat_compact.insert( _cov_mat.begin(), zeros.begin(), zeros.end() );

//
//void KinFitter::ExtendCovMat( const vector_d2 &cov_mat ){
//  // Extend the covariance C_eta with the input cov_mat 
//  
//  // Add padding to the rows
//  vector_d zeros( cov_mat.size(), 0 );
//  for( auto &row : C_eta ){ row.insert( row.end(), zeros.begin(), zeros.end() ); }
//
//  // Add columns with padding
//  int nrows = C_eta.size();
//  for( auto row : cov_mat ){ 
//    vector_d newrow( nrows, 0 );
//    newrow.insert( newrow.end(), row.begin(), row.end() );
//    C_eta.push_back( newrow ); 
//  }
//
//}
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void KinFitter::SetCovarianceMatrix( const vector_d2 &covMat ){ C_eta = covMat; }
  
  //int n_meas   = GetNMeasured();
  ////if( isCovMatDiag ){
  ////  //vector_d zeros(n_meas, 0);
  ////  for( int irow = 0; irow < n_meas; irow++ ){
  ////    vector_d zeros_right(n_meas-irow-1, 0);
  ////    vector_d row( irow, 0 );
  ////    row.push_back( cov_mat_compact.at(irow) );
  ////    result.insert( result.end(), row.begin(), row.end() ); 
  ////    result.insert( result.end(), zeros_right.begin(), zeros_right.end() ); 
  ////  }
  ////}
  ////else{
  //    //vector_d zeros_right(n_meas-irow-1, 0);
  //    //vector_d row( irow, 0 );
  //    //row.push_back( cov_mat_compact.at(icovmat) );
  //  vector_d2 result_2d = vector_d2( n_meas, vector_d( n_meas, 0 ) );
  //  //for( auto cc: cov_mat_compact ) cout << cc << " "; cout << endl;
  //  for( int irow = 0; irow < n_meas; irow++ ){
  //    //// nlast is the total number of elements above this row of pascal's triangle
  //    int nlast = ( irow * (irow + 1) )/2; 
  //    for( int icol = 0; icol < irow+1; icol++){
  //      int icovmat = nlast + icol;
  //      //cout << icovmat << endl;
  
  //      result_2d.at(irow).at(icol) = result_2d.at(icol).at(irow) = cov_mat_compact.at(icovmat); 
  //    }
  //  }
  //}
  
  //// std->eig
  //vector_d C_eta_std;
  //// Flatten C_eta
  //for( auto row : C_eta ) C_eta_std.insert( C_eta_std.end(), row.begin(), row.end() ); 
  //Map<MatrixXdRM> C_eta_eig( C_eta_std.data(), n_meas, n_meas  );
  //VectorXd C_eta_diag( C_eta.data(), n_meas );
  //Map<MatrixXd> C_eta_triag( cov_mat_compact.data(), n_meas, n_meas );
  //C_eta_eig = C_eta_triag;
  
    
    //Map<MatrixXd> C_eta_triag( cov_mat_compact.data(), n_meas, n_meas );
    //C_eta_eig = C_eta_triag;
//template<typename Constraints>
//constraints<typename std::decay<Constraints>::type> KinFitter::build_constraints(Constraints&& c){ return { std::forward<Constraints>(c) }; }

//template class constraints<typename std::decay<Constraints>::type>;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

