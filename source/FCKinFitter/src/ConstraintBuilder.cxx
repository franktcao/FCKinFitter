#include "ConstraintBuilder.h"

using std::cout;
using std::endl;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Constructor with extra constraints (other than energy and momentum conservation... like an invariant mass)
ConstraintBuilder::ConstraintBuilder( const function<vector<double>(const vector<double>&, const vector<double>&, int, int )>  &constraints_functs, int ncons )
  :
    n_constraints(ncons),
    eval_const( constraint_functs )
{
  //c.assign( ncons );
  //B.assign( ncons );
  //A.assign( ncons );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::tuple< vector<double>, vector<vector<double>>, vector<vector<double>> > ConstraintBuilder::GetCBA( const vector<double> &y, const vector<double> &x ){
  
  c = eval_constraints( y, x );
  B.clear();
  for( int ii = 0; ii < y.size(); ii++ ){ auto B_temp = eval_constraints( y, x, 1, ii ); B.insert( B.end(), B_temp.begin(), B_temp.end() );  
  A.clear();
  for( int ii = 0; ii < x.size(); ii++ ){ auto A_temp = eval_constraints( y, x, 0, ii ); A.insert( A.end(), A_temp.begin(), A_temp.end() );  
  
  return std::make_tuple( c, B, A );
  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//[&] vector<double>( const vector<double> &y, const vector<double> &x ){
//  TVector3 p3;
//  TLorentzVector P;
//  double M = KFParticles[ii].P.M();
//  p3.SetMagThetaPhi( y[ii+0], y[ii+1], y[ii+2] );
//  P.SetVectM( p3, M );
//  }
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//std::tuple< vector<double>, vector<vector<double>>, vector<vector<double>> > ConstraintBuilder::UpdateCBA( const vector<double> &y, const vector<double> &x ){
//  int n_vars_y = y.size();
//  int n_vars_x = x.size();
//  
//  vector<double>         c( n_constraints ); 
//  vector<vector<double>> B( n_constraints, vector<double>(n_vars_y) ); 
//  vector<vector<double>> A;
//  if( n_vars_x > 0 ) A = vector<double>( n_constraints, vector<double>(n_vars_x )       ); 
//  else               A = vector<double>( n_constraints, vector<double>(n_constraints )  ); // This size for multiplication reasons
//
//  
//  // Construct c from constraints
//  c_std = eval_constraints( y, x );  
//  
//  int i_part_y =0;
//  int i_part_x =0;
//  //for( auto KFP : KFParticlesIn ){
//  for( int ipart = 0; ipart < KFParticlesIn.size(); ipart++ ){
//    $
//    auto KFP = KFParticlesIn[ipart];
//    int ipart_yx = KFP.ipart_yx;
//    if( KFP.isMeas == 1 ){
//      // Construct B from constraints
//      for( int j = ipart_yx; j < ipart_yx + 3; j++ ){
//        B.at(j)  = eval_constraints( y, x, ipart, j%3 ); 
//      }
//      i_part_y++;
//    } 
//    else{
//      // Construct A from constraints
//      for( int j = ipart_yx; j < ipart_yx + 3; j++ ){
//        A.at(j)  = Map<VectorXd>( eval_constraints( y, x, ipart, j%3 ).data(), n_constraints ); 
//      }
//      i_part_x++;
//    }
//  }
//
//  return std::make_tuple( c, B, A );
//}
//

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //using der_arrays = vector< function< vector<double>( const vector<double>&, const vector<double>& ) > >;
  //der_arrays B_functs, A_functs;
  //eval_derivatives = [=]( const vector<double> &vec ){
  //  vector<double> vec;
  //  double h = 1E-5;
  //  for( int ivar = 0; ivar < y.size(); ivar++ ){
  //    y.at( ivar ) += h; 
  //    std::vector<double> f_y_hi = eval_constraints( y, x ); 
  //    y.at( ivar ) -= h; 

  //    y.at( ivar ) -= h; 
  //    std::vector<double> f_y_lo = eval_constraints( y, x ); 
  //    y.at( ivar ) += h; 

  //    for( int ii = 0; ii < y.size(); ii++ ) B.at( ivar ).at( ii ) = ( f_y_hi.at(ii) - f_y_lo.at(ii) ) / (2*h);
  //  }
  //  for( int ivar = 0; ivar < x.size(); ivar++ ){

  //    for( int ii = 0; ii < x.size(); ii++ ) A.at( ivar ).at( ii ) = ( f_x_hi.at(ii) - f_x_lo.at(ii) ) / (2*h);
  //  }

  //  for( int ivar = 0; ivar < y.size(); ivar++ ){
  //    B_functs.push_back( );
  //  }
  //  for( int ivar = 0; ivar < x.size(); ivar++ ){
  //    x.at( ivar ) += h; 
  //    std::vector<double> f_x_hi = eval_constraints( y, x ); 
  //    x.at( ivar ) -= h; 

  //    x.at( ivar ) -= h; 
  //    std::vector<double> f_x_lo = eval_constraints( y, x ); 
  //    x.at( ivar ) += h; 
  //    A_functs.push_back( );
  //  }
  //  

  //  return der;
 






  //eval_constraints = [&](const vector<double> &y, const vector<double> &x, int isMeas, int ivar){ 
  //  auto vec = constraint_functs(y, x, isMeas, ivar); 
  //  valarray<double> valarr; 
  //  std::copy( vec.begin(), vec.end(), valarr.begin() );
  //  return valarr;
  //};
  //
  ////std::for_each( );
  //

  //double h = 1E-5;
  //c = eval_constraints(y, x);
  //for( auto &yi : y ){
  //  std::valarray<double> dfdy;
  //  yi += h;
  //  dfdy  = std::valarray<double>( eval_constraints(y, x).data(), ncons );
  //  yi -= h;

  //  yi -= h;
  //  dfdy -= std::valarray<double>( eval_constraints(y, x).data(), ncons );
  //  yi += h;
  //  
  //  dfdy /= 2*h;
  //  unsigned ivar = &yi - &y.at(0);
  //  std::copy( dfdy.begin(), dfdy.end(), B.at(ivar).begin() );
  //}
  //for( auto &xi : x ){
  //  std::valarray<double> dfdx;
  //  xi += h;
  //  dfdx  = std::valarray<double>( eval_constraints(y, x).data(), ncons );
  //  xi -= h;

  //  xi -= h;
  //  dfdx -= std::valarray<double>( eval_constraints(y, x).data(), ncons );
  //  xi += h;
  //  
  //  dfdx /= 2*h;
  //  unsigned ivar = &xi - &x.at(0);
  //  std::copy( dfdx.begin(), dfdx.end(), A.at(ivar).begin() );
  //}
  //
  //auto B_functs = [&]( vector<double> &y, vector<double> &x ){
  //  for( auto &yi : y ){
  //    std::function< vector<double> (vector<double> &, vector<double> &) > dfdy;

  //    yi += h;
  //    auto dfdy = [&]( vector<double> &y, vector<double> &x ){ valarray<double> dfdy = eval_constraints(y_hi, x) - eval_constraints(y_lo, x); dfdy /= 2*h; return dfdy; };
  //    yi -= h;

  //    yi -= h;
  //    auto dfdy_lo = std::valarray<double>( eval_constraints(y, x).data(), ncons );
  //    yi += h;

  //    dfdy /= 2*h;
  //    unsigned ivar = &yi - &y.at(0);
  //    std::copy( dfdy.begin(), dfdy.end(), B.at(ivar).begin() );
  //  }
  //}
  
