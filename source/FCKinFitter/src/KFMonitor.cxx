#include "KFMonitor.h"

// Useful shortcuts
using namespace std;
using vector_d  = vector<double>;
using vector2_d = vector<vector<double>>;
using vector_h  = std::vector<TH1D>;
using vector2_h = std::vector<vector_h>;
using vector_s  = vector<string>;

//________________________________________________________________________________________________________________________________

KFMonitor::KFMonitor( KinFitter &_KF, double _clc, const TLorentzVector &_P_beam, const TLorentzVector &_P_targ )
  : clc(_clc)
{
  clc_left  = clc;
  clc_right = 1;
  // if clc is negative, we want the failed events
  if( clc < 0 ){ clc_right = -clc; clc_left = 0; clc *= -1; }
    
  n_meas   = _KF.GetNMeasured( );
  n_unmeas = _KF.GetNUnmeasured( );
  exc_vars_0.assign(9,0.);
  
  
  auto &kfps     = _KF.KFParticlesIn;
  kin = Kinematics( _P_beam, _P_targ );

  for( auto &kfp : kfps ){
    kfp_names.push_back( kfp.name );
    int count_meas   = kfp.GetNMeasured(); 
    int count_unmeas = kfp.GetNUnmeasured(); 
    if( n_meas_max   < count_meas   ) n_meas_max   = count_meas;
    if( n_unmeas_max < count_unmeas ) n_unmeas_max = count_unmeas;
    
    if( count_meas   != 0 ){ 
      ikfp_meas  .push_back( &kfp - &kfps.at(0) ); 
      n_meas_kfp .push_back( count_meas         );
      for( int ii = 0; ii < count_meas; ii++ ){
        string vname  = kfp.names_meas.at(ii); 
        string uname  = kfp.units_meas.at(ii); 
        std::pair<double,double> minmax = kfp.minmax_meas.at(ii);

        vnames_meas.push_back( vname  );
        unames_meas.push_back( uname  );
        minmax_meas.push_back( minmax );
      }
    } 
    if( count_unmeas != 0 ){ 
      ikfp_unmeas.push_back( &kfp - &kfps.at(0) ); 
      n_unmeas_kfp.push_back( count_unmeas );
      for( int ii = 0; ii < count_unmeas; ii++ ){
        string vname  = kfp.names_unmeas.at(ii); 
        string uname  = kfp.units_unmeas.at(ii); 
        std::pair<double,double> minmax = kfp.minmax_unmeas.at(ii);

        vnames_unmeas.push_back( vname  );
        unames_unmeas.push_back( uname  );
        minmax_unmeas.push_back( minmax );
      }
    }
  }

  InitHistograms( );

}

//________________________________________________________________________________________________________________________________

vector_kfp KFMonitor::GetKFParticles( const KinFitter &_KF, const vector_i &ikfps, int ifit ){
  vector_kfp result;
  vector_kfp kfps;

  if     ( ifit == 0 ) kfps = _KF.KFParticlesIn;
  else if( ifit == 1 ) kfps = _KF.KFParticlesOut;
  //int nkfps = ikfps.size();
  //std::vector<int> idx_kps = ikfps;
  //if( nkfps < 1 ){ idx_kfps.resize( kfps.size() ); iota( idx_kfps.begin(), idx_kfps.end(), 0 ); }

  //for( int ii = 0; ii < ikfps.size(); ii++ ){
  //  int ikfp = ikfps.at(ii);
  //  KFParticle &kfp = kfps.at(ikfp);
  
  for( auto p : kfps ) result.push_back( p ); 
  return result;
}

//________________________________________________________________________________________________________________________________

void KFMonitor::InitHistograms( ){
  InitConfLevels();
  InitPulls();
  InitKFVars();
  InitExcVars();
  Init2DHists();
}

//________________________________________________________________________________________________________________________________

void KFMonitor::InitExcVars( ){
  v2h_exc_vars.assign( 9, vector_h( 4, TH1D() ) );                             
  
  std::vector<TString> titles;
  std::vector<std::vector<double>> bins;
  

  TString beam = (TString) kfp_names.at(0);
  TString rec  = (TString) kfp_names.at(1);
  TString prod = (TString) kfp_names.at(2);
  
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + prod +              " X; M^{2}_{X}  [(GeV/c^{2})^{2}];");   bins.push_back({ 7  , 22   });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  +              " X; M^{2}_{X}  [(GeV/c^{2})^{2}];");   bins.push_back({-1  ,  1   });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  + " " + prod + " X; M^{2}_{X}  [(GeV/c^{2})^{2}];");   bins.push_back({-0.1,  0.1 });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  + " " + prod + " X; px_{X}    [GeV/c];"           );   bins.push_back({-0.2,  0.2 });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  + " " + prod + " X; py_{X}    [GeV/c];"           );   bins.push_back({-0.2,  0.2 });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  + " " + prod + " X; pt_{X}    [GeV/c];"           );   bins.push_back({ 0  ,  0.2 });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  + " " + prod + " X; E_{X}     [GeV];"             );   bins.push_back({-1  ,  1   });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  +              " X; #theta_{X, #pi^{0}} [deg.];"  );   bins.push_back({ 0  ,  3.5 });
  titles.push_back(beam + " " + rec + " #rightarrow " + beam + "  " + rec  + " " + prod + " X; #Delta #phi [deg.];"          );   bins.push_back({-5. ,  5.  });
  
  for( int ii = 0; ii < 9; ii++ ){
    TString title = titles.at(ii);
    double min = bins.at(ii).at(0);
    double max = bins.at(ii).at(1);
    for( int jj = 0; jj < 4; jj++ ){
      TH1D & hist = v2h_exc_vars.at(ii).at(jj);
      hist = TH1D( Form("exc_vars_%i_pass_%i", ii, jj), title, 80, min, max ); 
      hist.SetLineWidth( 2 ); 
      if     ( jj == 0 )  hist.SetLineColorAlpha( kBlack,   1.00 );
      else if( jj == 2 )  hist.SetLineColorAlpha( kRed+2,   1.00 );
      else if( jj == 3 ){ hist.SetLineColorAlpha( kGreen+2, 0.35 ); hist.SetFillColorAlpha( kGreen+2, 0.35 ); }
    }
  }

}

//________________________________________________________________________________________________________________________________

void KFMonitor::InitKFVars( ){
  v2h_meas  .assign( n_meas,   vector_h( 4, TH1D() ) );                             
  v2h_unmeas.assign( n_unmeas, vector_h( 4, TH1D() ) );                             
  
  double min, max;
  for( int ii = 0; ii < n_meas; ii++ ){
    TString vname = vnames_meas.at(ii); 
    TString uname = unames_meas.at(ii); 
    std::tie( min, max ) = minmax_meas.at(ii);
    for( int jj = 0; jj < v2h_meas.at(0).size(); jj++ ){
      TH1D & hist = v2h_meas.at(ii).at(jj);
      hist = TH1D( Form("meas_%i_pass_%i", ii, jj), vname + ";" +vname +" " + uname +  "; Counts/100", 100, min, max ); 
      if     ( jj == 0 )  hist.SetLineColorAlpha( kBlack,   1.00 );
      //else if( jj == 1 ){ hist.SetLineColorAlpha( kBlue,    0.35 ); hist.SetFillColorAlpha( kBlue,    0.35 ); }
      else if( jj == 1 ){ hist.SetLineWidth( 2 ); }
      else if( jj == 2 )  hist.SetLineColorAlpha( kRed+2,   1.00 );
      else if( jj == 3 ){ hist.SetLineColorAlpha( kGreen+2, 0.35 ); hist.SetFillColorAlpha( kGreen+2, 0.35 ); }
    }
  }

  for( int ii = 0; ii < n_unmeas; ii++ ){
    TString vname = vnames_unmeas.at(ii); 
    TString uname = unames_unmeas.at(ii); 
    double min    = minmax_unmeas.at(ii).first;
    double max    = minmax_unmeas.at(ii).second;
    for( int jj = 0; jj < v2h_unmeas.at(0).size(); jj++ ){
      TH1D & hist = v2h_unmeas.at(ii).at(jj);
      hist = TH1D( Form("unmeas_%i_pass_%i", ii, jj), vname + ";" +vname +" " + uname +  "; Counts/Bin", 100, min, max ); 
      if     ( jj == 0 ) hist.SetLineColorAlpha( kBlack,   1.00 );
      //else if( jj == 1 ){ hist.SetLineColorAlpha( kBlue,    0.35 ); hist.SetFillColorAlpha( kBlue,    0.35 ); }
      else if( jj == 1 ){ hist.SetLineWidth( 2 ); }
      else if( jj == 2 ) hist.SetLineColorAlpha( kRed+2,   1.00 );
      else if( jj == 3 ){ hist.SetLineColorAlpha( kGreen+2, 0.35 ); hist.SetFillColorAlpha( kGreen+2, 0.35 ); }
    }
  }
}

//________________________________________________________________________________________________________________________________

void KFMonitor::Init2DHists(){

  v2h2_clVpull.assign( n_meas,   vector_h2( 2, TH2D() ) );                             
  v2h2_dyVy   .assign( n_meas,   vector_h2( 2, TH2D() ) );                                                          
  v2h2_dxVx   .assign( n_unmeas, vector_h2( 2, TH2D() ) );                                                                                       

  double min, max;
  TString name, units, hname; 
  for( int ii = 0; ii < n_meas; ii++ ){
    name  = vnames_meas.at(ii);
    units = unames_meas.at(ii);
    std::tie( min, max ) = minmax_meas.at(ii);
    for( int jj = 0; jj < 2; jj++ ){
      hname = "Conf. Level vs. z_{"+name+"}; z_{"+name+"} [ ]; Confidence Level [ ];";
      v2h2_clVpull.at(ii).at(jj) = TH2D( Form("clVpull_%i_pass%i", ii, jj), hname, 15, -5,  5,   15, 0., 1. );
      hname = "#Delta "+name+"/"+name+ "vs "+name+ "; "+name+" "+units+"; #Delta "+name+"/"+name;
      v2h2_dyVy   .at(ii).at(jj) = TH2D( Form("dyOyVy_%i_pass%i",  ii, jj), hname, 15, min, max, 15, -1., 1. );
    }
  }
  for( int ii = 0; ii < n_unmeas; ii++ ){
    name  = vnames_unmeas.at(ii);
    units = unames_unmeas.at(ii);
    std::tie( min, max ) = minmax_unmeas.at(ii);
    for( int jj = 0; jj < 2; jj++ ){
      hname = "#Delta "+name+"/"+name+ "vs "+name+ "; "+name+" "+units+"; #Delta "+name+"/"+name;
      v2h2_dxVx.at(ii).at(jj) = TH2D( Form("dxOxVx_%i_pass%i", ii, jj), hname, 15, min, max, 15, -1., 1. );
    }
  }
}

//________________________________________________________________________________________________________________________________

void KFMonitor::InitConfLevels(){
  conf_levels.assign(2, TH1D());                       
  for( int ii = 0; ii < conf_levels.size(); ii++ ){
    TH1D & hist = conf_levels.at(ii);
    hist = TH1D( Form("clc_%i", ii) , "Confidence Level", 100, 0, 1 ); 
    if( ii == 1 ) hist.SetFillColorAlpha( kBlue, 0.35 );
  }
}

//________________________________________________________________________________________________________________________________

void KFMonitor::InitPulls( ){
  v2h_pulls.assign( n_meas, vector_h( 2, TH1D() ) );                             
  for( int ii = 0; ii < n_meas; ii++ ){
    for( int jj = 0; jj < v2h_pulls.at(0).size(); jj++ ){
      TString vname = vnames_meas.at(ii); 
      TH1D & hist = v2h_pulls.at(ii).at(jj);
      hist = TH1D( Form("pulls_%i_pass_%i", ii, jj), vname + " Pulls; z_{"+vname+"} [ ]; Counts/100", 100, -5, 5 ); 
      hist.SetMinimum(0);
      if( jj == 1 ) hist.SetFillColorAlpha( kBlue, 0.35 );
    }
  }
}

//________________________________________________________________________________________________________________________________

void KFMonitor::SetExcVars( int ifit, const vector_d &_exc_vars ){
  if     ( ifit == 0 ) exc_vars     = _exc_vars;
  else if( ifit == 1 ) exc_vars_fit = _exc_vars;
}

//________________________________________________________________________________________________________________________________

void KFMonitor::FillHistograms( KinFitter &_KF ){
  // Get values that will be used to fill 
  double cl       = _KF.confLevel;
  vector_d pulls  = _KF.pulls;
  vector_d meas   = _KF.eta;
  vector_d unmeas = _KF.x0;
  

  auto &kfps = _KF.KFParticlesIn;
  auto &kfps_fit = _KF.KFParticlesOut;
  
  TLorentzVector P_scat = kfps.at(0).GetP();
  TLorentzVector P_rec  = kfps.at(1).GetP();
  TLorentzVector P_prod = kfps.at(2).GetP();

  kin.UpdateKinematics( P_scat, P_rec, P_prod ); 
  exc_vars = kin.exc_vars.GetExclusivityVariables();
  
  exc_vars_0.at(0) = P_rec.M2();
  exc_vars_0.at(1) = P_prod.M2();
  
  P_scat = kfps_fit.at(0).GetP();
  P_rec  = kfps_fit.at(1).GetP();
  P_prod = kfps_fit.at(2).GetP();

  kin.UpdateKinematics( P_scat, P_rec, P_prod ); 
  exc_vars_fit = kin.exc_vars.GetExclusivityVariables();



  vector_d d_meas = _KF.epsilon;
  vector_d meas_fit = _KF.GetMeasured( 1 );
  for( int ii = 0; ii < d_meas.size(); ii++ ) d_meas[ii]   /= meas[ii]; 
  
  vector_d d_unmeas = unmeas;
  vector_d unmeas_fit = _KF.GetUnmeasured( 1 );
  for( int ii = 0; ii < d_unmeas.size(); ii++ ){ d_unmeas[ii] -= unmeas_fit[ii]; d_unmeas[ii] /= unmeas[ii]; }


  // Fill all (whether pass or fail fit)
  conf_levels.at(0).Fill( cl );
  for( int ii = 0; ii < meas.size(); ii++ ){
    v2h_pulls   .at(ii).at(0).Fill( pulls.at(ii) );  
    v2h_meas    .at(ii).at(0).Fill( meas .at(ii) );                             

    v2h2_clVpull.at(ii).at(0).Fill( pulls.at(ii),  cl            );                             
    v2h2_dyVy   .at(ii).at(0).Fill( meas.at(ii),   d_meas.at(ii) );                             
  }
  for( int ii = 0; ii < unmeas.size(); ii++   ){ 
    v2h_unmeas.at(ii).at(0).Fill( unmeas.at(ii) );                             

    v2h2_dxVx .at(ii).at(0).Fill( unmeas.at(ii), d_unmeas.at(ii) );                   
  }
  for( int ii = 0; ii < exc_vars.size(); ii ++ ){
    v2h_exc_vars.at(ii).at(0).Fill( exc_vars.at(ii) );                             
  }

  if( clc_left <= cl && cl <= clc_right ){
    // Fill pass fit
    conf_levels.at(1).Fill( cl );
    for( int ii = 0; ii < meas.size(); ii++ ){
      v2h_pulls   .at(ii).at(1).Fill( pulls   .at(ii) );  
      v2h_meas    .at(ii).at(1).Fill( meas    .at(ii) );                             
      v2h_meas    .at(ii).at(3).Fill( meas_fit.at(ii) );                             

      v2h2_clVpull.at(ii).at(1).Fill( pulls.at(ii),  cl            );                             
      v2h2_dyVy   .at(ii).at(1).Fill( meas.at(ii),   d_meas.at(ii) );                             
    }
    for( int ii = 0; ii < unmeas.size(); ii++ ){
      v2h_unmeas.at(ii).at(1).Fill( unmeas    .at(ii)  );                             
      v2h_unmeas.at(ii).at(3).Fill( unmeas_fit.at(ii)  );                             

      v2h2_dxVx .at(ii).at(1).Fill( unmeas.at(ii), d_unmeas.at(ii) );                            
    }
    for( int ii = 0; ii < exc_vars.size(); ii ++ ){
      v2h_exc_vars.at(ii).at(1).Fill( exc_vars.at(ii) );                             
      v2h_exc_vars.at(ii).at(3).Fill( exc_vars_fit.at(ii) );                             
    }

  }
  else{ 
    // Fill fail fit
    for( int ii = 0; ii < meas  .size(); ii++ ) v2h_meas  .at(ii).at(2).Fill( meas .at(ii) );                             
    for( int ii = 0; ii < unmeas.size(); ii++ ) v2h_unmeas.at(ii).at(2).Fill( unmeas.at(ii) );                             
    for( int ii = 0; ii < exc_vars.size(); ii ++ ) v2h_exc_vars.at(ii).at(2).Fill( exc_vars.at(ii) );                             
  }
}

//________________________________________________________________________________________________________________________________

void KFMonitor::DrawAll( ){
  gSystem->Exec("mkdir -p out/images/");
  gSystem->Exec("mkdir -p out/root/");
  gSystem->Exec("ln -sfn out/images");
  gSystem->Exec("mkdir -p images/pdf/indivs/");
  gSystem->Exec("mkdir -p images/indivs/");

  DrawConfidenceLevel();
  DrawPulls();
  DrawKFVars();
  DrawExcVars();
}


//________________________________________________________________________________________________________________________________

void KFMonitor::DrawConfidenceLevel(){
  TCanvas * c1 = new TCanvas; 
  // Draw Conf. Level

  TString pngname = "confLevels";
  TF1 * lfit = new TF1( "lfit", "pol0", 0.0, 1);

  TH1D & cl_all  = conf_levels.at(0);
  TH1D & cl_pass = conf_levels.at(1);
  cl_pass.Fit("lfit", "Q", "Q", clc + (1-clc)/2, 1.0);
  double plateau = lfit->GetParameter(0);
  //  double snr = calcSNR( h_confLevels, confCutL, plateau );
  //  double sigpct = calcSigPct( h_confLevels, confCutL, plateau );
  //  stringstream ss;
  //  ss.str("");
  //  ss << fixed << setprecision(2) << confCutL;
  //  TString clcut = ss.str();
  //  h_confLevels->SetTitle(  string(h_confLevels->GetTitle()) + " ( " + to_string((Int_t)h_confLevels_after->GetEntries()) + " events, est. SNR = " + to_string(snr) + ", sig. pct. = " + to_string(sigpct) + "% with conf. cut @ " + clcut + ") " );
  //

  cl_all.SetTitle( Form("%s (%.0f Events Passed); Confidence Levels [ ]; Counts/Bin [ ];", cl_all.GetTitle(), cl_pass.GetEntries()) ) ; //, (int) cl_pass.GetNbinsX())   );
  cl_all.Draw();
  c1->Update();
  gStyle->SetOptFit();
  DrawHLine( plateau, 1, 0.0, kRed, 1, 1); 
  DrawVLine( clc, 1.8*gPad->GetUymax()); 
  cl_pass.Draw("same");

  c1->SetLogy();
  c1->Update();
  PrintImages( c1, pngname, "images/" );
}

//________________________________________________________________________________________________________________________________

void KFMonitor::DrawPulls(  ){
  // Draw Pull Distributions

  TCanvas * c1 = new TCanvas("c1", "c1", n_meas_max*400, n_meas/n_meas_max*400);
  int nrows = n_meas_kfp.size();
  int ncols = n_meas_max;
  c1->Divide(ncols, nrows);
  int ipull = 0;
  for( int ipart = 0; ipart < nrows; ipart++ ){
    int nvar = n_meas_kfp[ipart];
    for( int ivar = 0; ivar < nvar; ivar++ ){
      int ipad = 1 + ncols*ipart + ivar;
      auto pad = c1->cd( ipad );
      //cout << v2h_pulls[ipull][0].GetTitle() << endl;

      // Actual fit to passed events
      //TF1 *gausFit = FitGaus( v2h_pulls.at(ipull).at(1) );
      auto func = []( double *v, double *par ){ 
        double arg = (v[0] - par[1])/par[2]; 
        double fitval = par[0]*TMath::Exp(-0.5*arg*arg); 
        return fitval; 
      };
      TF1 * gausFit = new TF1("gausFitt2", func, -5, 5, 3);
      gausFit->SetParameters( v2h_pulls.at(ipull).at(1).GetMaximum(), 0, 1);
      gausFit->SetLineColorAlpha( kBlue, 1.);
      gausFit->SetParNames( "N (normalization)", "#mu (mean)", "#sigma (width)");
      
      (v2h_pulls.at(ipull).at(1)).Fit("gausFitt2", "Q", "", -5, 5);
      gStyle->SetOptFit(1);
      v2h_pulls.at(ipull).at(0).Draw();
      v2h_pulls.at(ipull).at(1).Draw("same");
      gausFit->Draw("same");

      // What it should be 
      gausFit->SetParameters( gausFit->GetParameter(0), 0, 1);
      gausFit->SetLineColorAlpha( kRed, 0.35 );
      gausFit->Draw("same");
      pad->Update();
      ///PrintImages( c1, "pulls_" + (TString) to_string(ipart) + "_" + (TString) to_string(ivar), "images/", "indivs/" );
      ipull++;
    }
  }
  PrintImages( c1, "pulls", "images/" );
}

//________________________________________________________________________________________________________________________________

void KFMonitor::DrawKFVars(  ){ 
  // Draw Pull Distributions

  TCanvas * c1 = new TCanvas("c1", "c1", n_meas_max*400, n_meas/n_meas_max*400);
  int nrows = n_meas_kfp.size();
  int ncols = n_meas_max;
  c1->Divide(ncols, nrows);
  int iplot = 0;
  for( int ipart = 0; ipart < nrows; ipart++ ){
    int nvar = n_meas_kfp[ipart];
    for( int ivar = 0; ivar < nvar; ivar++ ){
      int ipad = 1 + ncols*ipart + ivar;
      auto pad = c1->cd( ipad );
      v2h_meas.at(iplot).at(1).Draw();
      v2h_meas.at(iplot).at(0).Draw("same");
      v2h_meas.at(iplot).at(2).Draw("same");
      v2h_meas.at(iplot).at(3).Draw("same");
      //v2h_meas.at(iplot).at(0).Draw("same");
      //v2h_meas.at(iplot).at(1).Draw("same");

      iplot++;
    }
  }
  PrintImages( c1, "measured", "images/" );

  iplot = 0;
  for( int ipart = 0; ipart < nrows; ipart++ ){
    int nvar = n_meas_kfp[ipart];
    for( int ivar = 0; ivar < nvar; ivar++ ){
      int ipad = 1 + ncols*ipart + ivar;
      auto pad = c1->cd( ipad );
      v2h2_clVpull.at(iplot).at(0).Draw("colz");
      iplot++;
    }
  }
  PrintImages( c1, "clVpull_0", "images/" );
  iplot = 0;
  for( int ipart = 0; ipart < nrows; ipart++ ){
    int nvar = n_meas_kfp[ipart];
    for( int ivar = 0; ivar < nvar; ivar++ ){
      int ipad = 1 + ncols*ipart + ivar;
      auto pad = c1->cd( ipad );
      v2h2_clVpull.at(iplot).at(1).Draw("colz");
      iplot++;
    }
  }
  PrintImages( c1, "clVpull_1", "images/" );

  if( n_unmeas_max < 1 ) return;
  c1 = new TCanvas("c2", "c2", n_unmeas_max*400, n_unmeas/n_unmeas_max*400);
  nrows = n_unmeas_kfp.size();
  ncols = n_unmeas_max;
  c1->Divide(ncols, nrows);
  iplot = 0;
  for( int ipart = 0; ipart < nrows; ipart++ ){
    int nvar = n_unmeas_kfp[ipart];
    for( int ivar = 0; ivar < nvar; ivar++ ){
      int ipad = 1 + ncols*ipart + ivar;
      auto pad = c1->cd( ipad );
      v2h_unmeas.at(iplot).at(1).Draw();
      v2h_unmeas.at(iplot).at(0).Draw("same");
      v2h_unmeas.at(iplot).at(2).Draw("same");
      v2h_unmeas.at(iplot).at(3).Draw("same");
      //v2h_unmeas.at(iplot).at(0).Draw("same");
      //v2h_unmeas.at(iplot).at(1).Draw("same");

      iplot++;
    }
  }
  PrintImages( c1, "unmeasured", "images/" );
}

//________________________________________________________________________________________________________________________________

void KFMonitor::DrawExcVars(){
  // Draw Exclusivity Variable Distributions

  TCanvas * c1 = new TCanvas("c1", "c1", 3*400, 3*400);
  //TLine* line = nullptr;
  auto draw_v_line = []( double val, double max, double min = 0 ){
    TLine* line = new TLine( val, min, val, max ); 
    line->SetLineColorAlpha(kRed, 1);
    line->SetLineStyle(8);
    line->Draw("same");
  };
  
  c1->Divide(3, 3);
  for( int ivar = 0; ivar < 9; ivar++ ){
    int ipad = 1 + ivar;
    auto pad = c1->cd( ipad );
    v2h_exc_vars.at(ivar).at(0).Draw("same");
    v2h_exc_vars.at(ivar).at(1).Draw("same");
    v2h_exc_vars.at(ivar).at(2).Draw("same");
    v2h_exc_vars.at(ivar).at(3).Draw("same");
    pad->Update();
    double max = gPad->GetUymax();
    double value = exc_vars_0.at(ivar);
    draw_v_line( value, max );
  }
  PrintImages( c1, "exc_vars", "images/" );

}

//________________________________________________________________________________________________________________________________

TF1 * KFMonitor::FitGaus( TH1D & h ){
  // Actual fit to the data
  auto func = []( double *v, double *par ){ 
    double arg = (v[0] - par[1])/par[2]; 
    double fitval = par[0]*TMath::Exp(-0.5*arg*arg); 
    return fitval; 
  };
  TF1 * gausFit = new TF1("gausFitt2", func, -5, 5, 3);
  gausFit->SetParameters( h.GetMaximum(), 0, 1);
  gausFit->SetLineColorAlpha( kBlue, 1.);
  gausFit->SetParNames( "N (normalization)", "#mu (mean)", "#sigma (width)");
  h.Fit("gausFitt2", "Q", "", -5, 5);
  gStyle->SetOptFit(1);
  return gausFit;
}

//________________________________________________________________________________________________________________________________

void KFMonitor::PrintImages( TCanvas* canvas, TString outputName, TString outputDir, TString outputSubDir ){
  // Print canvas to png to outputDir then prints pdf in outputDir/pdf
  TString suffix;

  suffix = ".png";
  canvas->Print( outputDir + outputSubDir + outputName + suffix, suffix );
  suffix = ".pdf";
  canvas->Print( outputDir + "pdf/" + outputSubDir + outputName + suffix, suffix);
}

//_________________________________________________________________________________________________________

void KFMonitor::DrawVLine( Double_t value, Double_t max, Double_t min , EColor color , Int_t thickness , Int_t istyle , Float_t pct ){
  // Draw vertical line on the canvas at a certain x-value, value, from zero to max of that histogram, max

  TLine* line = new TLine( value, min, value, max ); 
  line->SetLineColorAlpha(color, pct);
  line->SetLineWidth(thickness);

  line->SetLineStyle(istyle);
  line->Draw("same");
}

//_________________________________________________________________________________________________________

void KFMonitor::DrawHLine( Double_t value, Double_t max, Double_t min , EColor color , Int_t thickness , Int_t istyle ){
  // Draw vertical line on the canvas at a certain x-value, value, from zero to max of that histogram, max

  TLine* line = new TLine( min, value, max, value ); 
  line->SetLineColor(color);
  line->SetLineWidth(thickness);

  line->SetLineStyle(istyle);
  line->Draw("same");
}

//_________________________________________________________________________________________________________

void KFMonitor::DrawVLines( Double_t left, Double_t right, Double_t max, Double_t min , EColor color , Int_t thickness , Float_t pct ){
  // Draw vertical lines to the left and right from zero to max of the histogram

  DrawVLine( left , max, min, color, thickness, 2, pct );
  DrawVLine( right, max, min, color, thickness, 9, pct );
}

//_________________________________________________________________________________________________________

void KFMonitor::DrawHLines( Double_t bot, Double_t top, Double_t max, Double_t min , EColor color , Int_t thickness ){
  // Draw vertical lines to the left and right from zero to max of the histogram

  DrawHLine( bot, max, min, color, thickness, 2 );
  DrawHLine( top, max, min, color, thickness, 9 );
}

////________________________________________________________________________________________________________________________________








//std::vector<TH1D*> getExcVarHistos(int istyle, double M2_targ){
//  std::vector<TH1D*> result;
//  TH1D h_excvar( "exc_var", "Exc. Var ", 100, -5, 5 );
//  std::vector<TString> excvars_titles = { "M_{X_{^{4}He}}^{2}", "M_{X_{#pi^{0}}}^{2}", "M_{X_{0}}^{2}", "px_{X_{0}}", 
//                                          "py_{X_{0}}", "pt_{X_{0}}", "E_{X_{0}}","#theta_{#pi^{0}, X_{#pi^{0}} }", "#Delta#phi" };
//  std::vector<TString> excvars_units = { "[ (GeV/c^{2})^{2} ]", "[ (GeV/c^{2})^{2} ]", "[ (GeV/c^{2})^{2} ]", "[ GeV/c ]", "[ GeV/c ]", "[ GeV/c ]", "[ GeV ]", "[ deg. ]", "[ deg. ]" };
//  std::vector<double> excvars_mins = { 0.5*M2_targ, -1, -0.06, -0.2, -0.2, 0.0, -1, 0, -5 };
//  std::vector<double> excvars_maxs = { 1.5*M2_targ,  1,  0.06,  0.2,  0.2, 0.2,  1, 3,  5 };
//  for( Int_t ii = 0; ii < 9; ii++){
//    TH1D    *hist = (TH1D*) h_excvar.Clone();
//    TString name  = string( hist->GetName() );
//    TString title = excvars_titles[ii];
//    hist->SetBins(120, excvars_mins[ii], excvars_maxs[ii]);
//    
//    if( istyle == 0 ){
//      hist->SetName( name + to_string(ii) );
//      hist->SetTitle( title + "; " + title + excvars_units[ii] );
//      hist->SetLineColorAlpha(kBlack, 1);
//    }
//    else if( istyle == 1 ){
//      hist->SetName( name + to_string(ii) + "_1" );
//      hist->SetTitle( title  + " (After CLC)" + "; " + title + excvars_units[ii]);
//      hist->SetLineColorAlpha(kBlue+2, 1);
//    }
//    else if( istyle == 2 ){
//      hist->SetName( name + to_string(ii) + "_fit" );
//      hist->SetTitle( title + " (fit)"+ "; " + title + excvars_units[ii] );
//      hist->SetFillColorAlpha(kGreen, 0.35);
//    }
//    result.push_back(hist);
//  }
//  return result;
//}

