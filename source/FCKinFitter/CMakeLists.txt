cmake_minimum_required (VERSION 3.1)

# Add -O0 to remove optimizations when using gcc
IF(CMAKE_COMPILER_IS_GNUCC)
  set(CMAKE_CXX_FLAGS "-O3")        ## Optimize
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O3")
  set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O3")
ENDIF(CMAKE_COMPILER_IS_GNUCC)

add_library(FCKinFitter SHARED 
  src/KinFitter.cxx
  src/KFParticle.cxx
  src/KFMonitor.cxx
  src/Kinematics.cxx
  )

set(lib_HEADERS 
  include/KFMonitor.h
  include/KFParticle.h
  include/KinFitter.h
  include/Kinematics.h
  )

#set(CMAKE_CXX_FLAGS "-Wall -Wextra")
#set(CMAKE_CXX_FLAGS_DEBUG "-g")
#set(CMAKE_CXX_FLAGS_RELEASE "-O3")
target_include_directories(FCKinFitter
    PUBLIC 
        $<INSTALL_INTERFACE:include>    
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
    PUBLIC ${ROOT_INCLUDE_DIRS}
    PUBLIC ${EIGEN3_INCLUDE_DIRS}
)

target_compile_features(FCKinFitter PUBLIC cxx_std_17)

target_compile_options(FCKinFitter 
  PUBLIC "-ftemplate-depth=5000" 
  PUBLIC "-fconcepts"
  PUBLIC "-O3"
)

target_link_libraries(FCKinFitter
  PUBLIC ${ROOT_LIBRARIES}
)

install(FILES 
    include/KinFitter.h 
    include/KFParticle.h 
    include/KFMonitor.h 
    include/Kinematics.h 
    include/res-errs_clas.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/FCKinFitter
    )

include(GNUInstallDirs)
  install(TARGETS FCKinFitter
      EXPORT FCKinFitterTargets
      LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
      ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
      )

## -----------------------------------------------
## Tests
## -----------------------------------------------
#set(test_name test_kfparticle)
#add_executable(${test_name} test/${test_name}.cxx)
### I add dis -- Frank
##set_target_properties(${test_name} PROPERTIES LINKER_LANGUAGE C)
#target_include_directories(${test_name} PUBLIC
#  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
#  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
#  ${CMAKE_INSTALL_INCLUDEDIR}/include
#  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
#  )
#target_link_libraries(${test_name}
#  PRIVATE KinFitter
#  PUBLIC stdc++
#  PUBLIC m
#  #PRIVATE InSANEnew_xsec
#  PUBLIC ${ROOT_LIBRARIES}
#  PRIVATE Catch)
#target_include_directories(${test_name}
#    PUBLIC 
#    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
#    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
#    PUBLIC ${EIGEN3_INCLUDE_DIRS}
#    PUBLIC ${ROOT_INCLUDE_DIRS}
#)
#target_compile_features(${test_name} PUBLIC cxx_std_17)
#add_test(NAME ${test_name} COMMAND ${test_name})
#
##set(test_name testkinfits_coh_dvcs)
##add_executable(${test_name} test/${test_name}.cxx)
#### I add dis -- Frank
###set_target_properties(${test_name} PROPERTIES LINKER_LANGUAGE C)
##target_include_directories(${test_name} PUBLIC
##  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
##  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
##  ${CMAKE_INSTALL_INCLUDEDIR}/include
##  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
##  )
##target_link_libraries(${test_name}
##  PRIVATE KinFitter
##  PUBLIC stdc++
##  PUBLIC m
##  #PRIVATE InSANEnew_xsec
##  PUBLIC ${ROOT_LIBRARIES}
##  PRIVATE Catch)
##target_include_directories(${test_name}
##    PUBLIC 
##    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
##    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
##    PUBLIC ${EIGEN3_INCLUDE_DIRS}
##    PUBLIC ${ROOT_INCLUDE_DIRS}
##)
##target_compile_features(${test_name} PUBLIC cxx_std_17)
##add_test(NAME ${test_name} COMMAND ${test_name})
#
#
##set(test_name TeSt)
##add_executable(${test_name} test/${test_name}.cxx)
##target_include_directories(${test_name} PUBLIC
##  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
##  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
##  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
##  )
##target_link_libraries(${test_name}
##  PRIVATE KinFitter
##  #PRIVATE InSANEnew_xsec
##  PUBLIC ${ROOT_LIBRARIES}
##  PRIVATE Catch)
##target_include_directories(${test_name}
##    PUBLIC 
##        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
##    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
##    PUBLIC ${EIGEN3_INCLUDE_DIRS}
##    PUBLIC ${ROOT_INCLUDE_DIRS}
##)
##target_compile_features(${test_name} PUBLIC cxx_std_17)
##add_test(NAME ${test_name} COMMAND ${test_name})
#
#
#
