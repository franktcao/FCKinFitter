#ifndef KFMonitor_h
#define KFMonitor_h
#include <iostream>
//#include <functional>
#include <string>

#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include "TCanvas.h"
#include "TLine.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TObject.h"
#include "TMath.h"
#include "KinFitter.h"
#include "Kinematics.h"

// Useful aliases
using std::cout;
using std::endl;
using std::string;

using constraint_t = std::function<vector_d(const vector_d&, const vector_d& )>;

using vector_i     = std::vector<int>;
using vector_d     = std::vector<double>;
using vector_h     = std::vector<TH1D>;
using vector_p     = std::vector<std::pair<double,double>>;
using vector2_h    = std::vector<vector_h>;
using vector_func  = std::vector<constraint_t>;

using vector_h2   = std::vector<TH2D>;
using vector2_h2  = std::vector<vector_h2>;

using vector_s    = std::vector<string>;
using vector_kfp  = std::vector<KFParticle>;

class KFMonitor : public TObject{
  
  public:

    int         n_meas;                           // Number of measured kin. fit variables
    int         n_unmeas;                         // Number of unmeasured kin. fit variables
    int         n_meas_max   = 0;                 // Max number of measured kin. fit variables from KFParticle
    int         n_unmeas_max = 0;                 // Max number of measured kin. fit variables from KFParticle
    double      clc          = 0;                 // Confidence level cut
    double      clc_left     = 0;
    double      clc_right    = 1;
    
  
    
    std::vector<std::function<vector_d(const vector_d&, const vector_d& )>> set_of_constraints;
    //vector_func set_of_constraints;
    vector_kfp  KFPs_exp;                         // Measured and calculated variables from experiment (for exclusivity plots)
    vector_kfp  KFPs_fit;                         // Fitted variables from fit (for exclusivity plots)


    // Might not need these below... 
    vector_i    ikfp_meas;                        // Vector of indices for measured particles from original KFParticles
    vector_i    ikfp_unmeas;                      // Vector of indices for unmeasured particles from original KFParticles
    
    vector_i    n_meas_kfp;                       // Vector holding number of Measured variables for each particle
    vector_i    n_unmeas_kfp;                     // Vector holding number of Unmeasured variables for each particle
    
    vector_d    exc_vars_0;                       // What the exclusivity varibles should be
    vector_d    exc_vars;                         // Measured exclusivity varibles 
    vector_d    exc_vars_fit;                     // Fitted exclusivity varibles
    vector_p    minmax_meas;                      // Vector holding number of Measured variables for each particle
    vector_p    minmax_unmeas;                    // Vector holding number of Unmeasured variables for each particle
    
    vector_s    kfp_names;                        // Vector holding names of particles 
    vector_s    vnames_meas;                      // Vector holding names of measured variables 
    vector_s    vnames_unmeas;                    // Vector holding names of unmeasured variables
    vector_s    unames_meas;                      // Vector holding units of measured variables
    vector_s    unames_unmeas;                    // Vector holding units of unmeasured variables
    
    // 2 different topologies ( 0: all, 1: pass clc measured)
    vector_h    conf_levels;                       
    vector2_h   v2h_pulls;                             
    
    // 4 different topologies ( 0: all, 1: pass clc measured, 2: fail clc measured, 3: pass clc fit vals )
    vector2_h   v2h_meas;                             
    vector2_h   v2h_unmeas;                             
    vector2_h   v2h_exc_vars;

    
    // 2 different topologies ( 0: all, 1: pass clc measured)
    vector2_h2  v2h2_clVpull;                             
    vector2_h2  v2h2_dyVy;                             
    vector2_h2  v2h2_dxVx;                             

    Kinematics kin;

  public:
    
    KFMonitor (){};
    KFMonitor( KinFitter &_k, double _clc, const TLorentzVector &_P_beam, const TLorentzVector &_P_targ );
    ~KFMonitor(){};
    
    void FillHistograms( KinFitter &_KF );

    void DrawAll();
    void DrawConfidenceLevel();
    void DrawPulls();
    void DrawKFVars();
    void DrawExcVars();
    
    void SetExcVars( int ifit, const vector_d &_exc_vars );
    
  private:
    
    void InitHistograms( );
    void InitPulls( );
    void InitConfLevels();
    void InitKFVars( );
    void InitExcVars( );
    void Init2DHists( );

    void PrintImages( TCanvas* canvas, TString outputName, TString outputDir = "", TString outputSubDir = ""  );
    void DrawVLine( Double_t value, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1, Int_t istyle = 8, Float_t pct = 1 );
    void DrawHLine( Double_t value, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1, Int_t istyle = 8  );
    void DrawVLines( Double_t left, Double_t right, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1, Float_t pct = 0.45);
    void DrawHLines( Double_t bot, Double_t top, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1);
    
    vector_kfp GetKFParticles( const KinFitter &_KF, const vector_i &ikfps = {}, int ifit = 0 );
    TF1 * FitGaus( TH1D & h );

};


#endif
    

