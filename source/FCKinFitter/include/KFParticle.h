#ifndef KFParticle_h
#define KFParticle_h
#include <iostream>
#include <functional>
#include <string>

#include "TObject.h"
#include "TLorentzVector.h"
#include "TMath.h"

// Useful aliases
using std::cout;
using std::endl;
using std::string;
using vector_d    = std::vector<double>;
using vector_d2   = std::vector<vector_d>;
using vector_s    = std::vector<string>;
using pair_d      = std::pair<double,double>;
using vector_p    = std::vector<pair_d>;
using kfvars_t    = std::tuple< vector_d, vector_d >;

class KFParticle : public TObject{

  
  public:

    string                   name;                      // Particle name or symbol
    int                      n_meas;                    // Number of measured kin. fit variables
    int                      n_unmeas;                  // Number of unmeasured kin. fit variables
    TLorentzVector           P;                         // 4-momentum
    TVector3                 V;                         // Vertex
    
    vector_s                 names_meas;                // The kin. fit measured variables' names  //= {"p", "#theta", "#phi"};        
    vector_s                 units_meas;                // The kin. fit measured variables' units  //= {"[GeV/c]", "[rad.]", "[rad.]"};
    vector_p                 minmax_meas;               // The kin. fit measured variables' minimums and maximums for histograms

    vector_s                 names_unmeas;              // The kin. fit unmeasured variables' names  //= {"p", "#theta", "#phi"};        
    vector_s                 units_unmeas;              // The kin. fit unmeasured variables' units  //= {"[GeV/c]", "[rad.]", "[rad.]"};
    vector_p                 minmax_unmeas;             // The kin. fit unmeasured variables' minimums and maximums for histograms

    
    vector_d                 cov_mat;                   // Compact Covariance Matrix ( 1 dimensional vector of either relevant half symmetric matrix or diagonal elements )
    bool                     isCovMatDiag;              // Is the covariance matrix diagonal? Interprets cov_mat as:
                                                        //      diagonal elements c_11, c_22, ... if isCovMatDiag == true 
                                                        //   or
                                                        //      symmetric elements c_11, c_12, c_22, ... if isCovMatDiag == false
    
    // Helper members
    int                      ipart_KFP;                 // After being added by the KinFitter, this will tell you which particle it is in the Kin. Fitter
    int                      ipart_y  ;                 // After being added by the KinFitter, this will tell you which particle it is in the meas. vector y
    int                      ipart_x  ;                 // After being added by the KinFitter, this will tell you which particle it is in the unmeas. vector x


    /** 
      This KFit defaults to kin. fit variables and width of p, theta, and phi.
     */

    //std::function< kfvars_t( const TLorentzVector&, const TVector3& ) > GetKFVars ;
    //std::function< std::tuple< TLorentzVector, TVector3 >( const vector_d&, const vector_d&, const TLorentzVector& ) > GetPhysVars;
    std::function< std::tuple< std::vector<double>, std::vector<double>  >( const TLorentzVector&, const TVector3& ) > GetKFVars ; // root needs the type of all data members' of a class to be spelled out in full
    std::function< std::tuple< TLorentzVector, TVector3 >( const std::vector<double>&, const std::vector<double>&, const TLorentzVector& ) > GetPhysVars; // root needs the type of all data members' of a class to be spelled out in full

  public:
    
    KFParticle (){};
    ~KFParticle(){};

    KFParticle( const string &_name, 
                int _isMeas               = 1, 
                const TLorentzVector &_P  = TLorentzVector(), 
                const TVector3 &_V        = TVector3(), 
                const vector_d &_widths   = {} 
    );

    
    void           SetParticleDefaults();
    void           SetParticleToUnmeasured();
    void           SetP( const TLorentzVector &PP );  
    void           SetV( const TVector3 &_V );
    void           SetCovMat( const vector_d  &_kfvars_sigmas, bool _isDiag = false ); // When you don't have a full covariance matrix but an estimate, using sigmas
    void           SetCovMat_triag(  const vector_d &_cov_mat_triag  ); 
    void           SetCovMat_diag(   const vector_d &_cov_mat_diag   );   // When you don't have a full covariance matrix but an estimate (sigma^2)
    void           SetCovMat_sigmas( const vector_d &_sigmas         );   // When you don't have a full covariance matrix but an estimate, using sigmas
    void           SetPAndCovMat( const TLorentzVector &_P, const vector_d &_cov_mat );  
    void           SetPAndCovMat_diag( const TLorentzVector &_P, const vector_d &_cov_mat );  
    void           SetPAndSigmas( const TLorentzVector &_P, const vector_d &_sigmas );  
    void           SetPVAndCovMat( const TLorentzVector &_P, const TVector3 &_V, const vector_d &_cov_mat );  
    void           SetName( const string &n );
    void           SetSigNames(  );
    void           SetMinMaxMeas( const vector_p &minmax);
    void           SetMinMaxUnmeas( const vector_p &minmax);
    void           SetProperties( 
                     const std::function< std::tuple<std::vector<double>, std::vector<double> >( const TLorentzVector&,      const TVector3& ) >                                   &_kf2physvars,
                     const std::function< std::tuple<TLorentzVector,      TVector3            >( const std::vector<double>&, const std::vector<double>&, const TLorentzVector& ) > &_phys2kfvars,
                     const std::vector<std::string>              &_names_meas    = {},
                     const std::vector<std::string>              &_units_meas    = {},
                     const std::vector<std::pair<double,double>> &_minmax_meas   = {},
                     const std::vector<std::string>              &_names_unmeas  = {},
                     const std::vector<std::string>              &_units_unmeas  = {},
                     const std::vector<std::pair<double,double>> &_minmax_unmeas = {}
        );
    
    int            GetNKFVars();                        // Get number of kin. fit var's.
    int            GetNMeasured();                      // Get number of measured kin. fit var's.
    int            GetNUnmeasured();                    // Get number of unmeasured kin. fit var's.
    int            GetIndexY0();                        // Get the first index of where the measured variables are in y of KinFitter class
    int            GetIndexX0();                        // Get the first index of where the unmeasured variables are in x of KinFitter class
    vector_d2      Get2DCovMat();                       // Get 2D matrix of covariance matrix

    vector_d       GetMeasured( );                      // Get measured kin. fit var's.
    vector_d       GetUnmeasured( );                    // Get unmeasured kin. fit var's.
    
    double         GetM ( );                                   
    double         GetVz( );                                   
    
    TLorentzVector GetP ( );                                   
    TVector3       GetV ( );                                             
   

    TLorentzVector GetP( const vector_d &_meas, const vector_d &_unmeas );
    TVector3       GetV( const vector_d &_meas, const vector_d &_unmeas );
    //TLorentzVector GetDP( const vector_d & _meas, const vector_d & _unmeas, int _ismeas, int _ivar );  // Get Derivative of Momentum w.r.t. kin. fit vars    
    //TVector3       GetDV( const vector_d & _meas, const vector_d & _unmeas, int _ismeas, int _ivar );  // Get Derivative of Vertex w.r.t. kin. fit vars   
    
    //std::tuple<TLorentzVector, TVector3> GetPhysVarsDers( vector_d &_meas, vector_d &_unmeas, int _ismeas, int _ivar );
};


#endif



