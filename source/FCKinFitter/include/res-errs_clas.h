#ifndef res_errs_clas_eg6_h 
#define res_errs_clas_eg6_h 

#include <iostream>
#include <cmath>
#include <cerrno>
#include <cfenv>
#include <cfloat>
#include <cstring>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// DC: Electron and Proton resolutions
std::vector<double> getResErr_DC( const TLorentzVector &P_e ){
  // Smearing Factors
  const double SF_p1    = 3.4;        // [ ]
  const double SF_p2    = 1.5;        // [ ]
  const double SF_theta = 2.5;        // [ ]
  const double SF_phi   = 4.0;        // [ ]
  // Parameters to characterize the resolutions in p
  const double A_p      = 35.;         // [deg.]   (Nominal theta in degrees)
  const double B_p      = 0.7;         // [ ]      (Power law in theta/nominal theta)
  const double C_p      = 3375.;       // [A]      (Nominal Torroid Current)
  //const double D_p      = 1900.;       // [A]      (EG6 Torroid Current)
  const double D_p      = 2099.;       // [A]      (E1-DVCS2 Torroid Current)
  const double E_p      = 0.0033;      // [1/GeV]  
  const double F_p      = 0.0018;      // [ ]
  // Parameters to characterize the resolutions in theta
  const double A_th     = 0.55/1000.;  // [rad]   
  const double B_th     = 1.39/1000.;  // [GeV rad]  
  // Parameters to characterize the resolutions in phi
  const double A_ph     = 3.73/1000;   // [rad]   
  const double B_ph     = 3.14/1000;   // [GeV rad]   


  const double p         = P_e.P();                         // [GeV/c]
  const double theta_deg = P_e.Theta() * TMath::RadToDeg(); // [deg.]
  const double beta      = P_e.Beta();                      // [ ]

  // First parentheses are Smearing and parametrizations introduced from fastmc studies
  // After are the resolution functions from the Mecking paper
  //const double sigma_p     = p * ( SF_p *  pow( theta_deg/A_p, B_p ) ) * C_p/D_p * std::hypot( E_p * p, F_p / beta );  // [GeV/c]
  const double sigma_p     = p * ( SF_p1/SF_p2 ) *  pow( theta_deg/A_p, B_p ) * C_p/D_p * std::hypot( E_p * p, F_p / beta );  // [GeV/c]
  const double sigma_theta =     ( SF_theta                          ) * std::hypot( A_th, B_th/p/beta );              // [rad]
  const double sigma_phi   =     ( SF_phi                            ) * std::hypot( A_ph, B_ph/p/beta );              // [rad]

  //std::cout << sigma_p << std::endl;
  return {sigma_p, sigma_theta, sigma_phi};
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IC: Photon resolutions
std::vector<double> getResErr_IC( const TLorentzVector &P_phot, double vz = 0 ){
  // Smearing Factors
  const double SF_E = 1.33;   // [ ]
  const double dX   = 1.20;   // [cm]  (Position resolution)
  // Parameters to characterize the resolutions in p
  const double A_E  = 0.024;  // [ ]   
  const double B_E  = 0.033;  // [(GeV/c)^1/2]  
  const double C_E  = 0.019;  // [GeV/c] 
  // Parameters to characterize the resolutions in theta
  const double A_th = 0.003;  // [((GeV/c)^1/2) / cm]   
  const double B_th = 0.013;  // [1/rad. cm]     
  // Parameters to characterize the resolutions in phi
  const double A_ph = 0.003;  // [((GeV/c)^1/2) / cm]   

  double E     = P_phot.E();      // [GeV]
  double theta = P_phot.Theta();  // [rad]
  
  const double sigma_E     = E * SF_E * std::hypot(A_E, B_E/sqrt(E), C_E/E); // [GeV]
  const double sigma_theta = dX * std::hypot(A_th/sqrt(E), B_th*theta);      // [rad]
  const double sigma_phi   = dX * A_ph/sqrt(E);                              // [rad]
  return {sigma_E, sigma_theta, sigma_phi};

  //// 4/16/18 Added to match fastmc.C
  //// Other
  //const double dVz_e       = 0.30;    // cm         !?
  //const double ric=tan(theta)*vz;
  //const double dthedz=-ric/(vz*vz+ric*ric);
  //const double sigthe=dthedz*dVz_e;
  //const double sigma_theta = dX * std::hypot(A_th/sqrt(E), B_th*theta) + sigthe;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// RTPC: Helium resolutions
std::vector<double> getResErr_RTPC( const TLorentzVector &P_he4 ){

  //// Resolutions
  const double dpOp   = 0.10;                               // [ ]        (dp/p)
  const double dtheta = 4.00;                               // [deg.]
  const double dphi   = 4.00;                               // [deg.]

  const double p      = P_he4.P();                          // [GeV/c]
  
  const double sigma_p     = dpOp   * p;                    // [GeV/c]
  const double sigma_theta = dtheta * TMath::DegToRad();    // [rad]
  const double sigma_phi   = dphi   * TMath::DegToRad();    // [rad]

  return {sigma_p, sigma_theta, sigma_phi};
  
  ////// Changed 4/16/18 to match Mohammad
  //const double dphi    = 2.00;    // [deg.]
  //const double dvz_he4 = 0.46;    // [cm] !!
  //const double beta  = P_he4.Beta();
  //const double theta = P_he4.Theta() * TMath::RadToDeg();

  //// 4/16/18 Added to match fastmc.C
  //static const double R=4.5;
  //static const double sigmaZ=0.46; // cm
  //const double dthedz=sin(2*theta)/R;
  //const double sigma_theta=fabs(sigmaZ*dthedz)+0.8*TMath::DegToRad();

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> getResErr_EC( const TLorentzVector &P_phot ){

  const double E     = P_phot.E();            // [GeV/c]
  
  const double sigma_E     = 0.116 * sqrt(E); // [GeV/c]
  const double sigma_theta = 0.004;           // [rad]
  const double sigma_phi   = 0.004;           // [rad]

  return {sigma_E, sigma_theta, sigma_phi};
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> getRecErrors( Int_t itarg, const TLorentzVector &P_rec ){
  if( itarg == 0 )      return getResErr_RTPC(P_rec);
  else if( itarg == 1 ) return getResErr_DC(P_rec);
  else                  std::cout << "no have " << itarg << std::endl; return {};
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> getPhotErrors( Int_t detID, const TLorentzVector &P_phot, double vz_e = 0 ){
  if( detID == 1 )      return getResErr_EC( P_phot );
  else if( detID == 2 ) return getResErr_IC( P_phot, vz_e );
  else                  std::cout << "no have detID" << detID << std::endl; return {};
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
